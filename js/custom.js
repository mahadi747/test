$(document).ready(function(){
  $("#top-slider").owlCarousel({
      nav:true,
      navText:['<i class="fa fa-angle-double-left" aria-hidden="true"></i>','<i class="fa fa-angle-double-right" aria-hidden="true"></i>'],
      autoplay:true,
      loop:true
  });

    //font size zoom
    var i=1;
    $(".zoom-in").click(function(){
        var inc=i++;
        var news_details_font_size=$(".news-details").css('font-size');
        var font_size=news_details_font_size.replace('px','');
        var num_font_size=Number(font_size);
        $(".news-details p").css('font-size',num_font_size+inc+'px');
    });
    $(".zoom-out-icon").click(function(){
        var inc=i--;
        var news_details_font_size=$(".news-details").css('font-size');
        var font_size=news_details_font_size.replace('px','');
        var num_font_size=Number(font_size);
        $(".news-details p").css('font-size',num_font_size+inc+'px');
    });
    
        
});

// v3.1.0
//Docs at http://simpleweatherjs.com
$(document).ready(function() {
  $.simpleWeather({
    location: 'Austin, TX',
    woeid: '',
    unit: 'c',
    success: function(weather) {
      html = '<i class="icon-'+weather.code+'"></i> '+weather.temp+'&deg;'+weather.units.temp;
      $("#weather").html(html);
    },
    error: function(error) {
      $("#weather").html('<p>'+error+'</p>');
    }
  });
});

//////hover popup info a single news
$(document).ready(function() {
  $(".popup-parent a").hover(function(){
      var news_id=$("input",this).val();
      $.post(
        "popup.php",
          {
              id:news_id
          },
          function(data,status){
              $(".popup").html(data);
          }
      );
  });  
  $(".popup-parent a").mouseleave(function(){
      $(".popup-child").remove();
  });  
    
  $(".popup-parent a").mousemove(function(element){
      var x=element.pageX;
      var y=element.pageY;
      var left=x;
      var top=y;
      $(".popup-child").show().css({"top":top+20,"left":left+20});
  });

});

//rating plugin 
$(document).ready(function() {
  $(".rate1").hover(function(){
      $(this).css("color","gold");
      $(".rate2,.rate3,.rate4,.rate5").css("color","black");
  });
  $(".rate2").hover(function(){
      $(".rate1,.rate2").css("color","gold");
      $(".rate3,.rate4,.rate5").css("color","black");
  });
  $(".rate3").hover(function(){
      $(".rate1,.rate2,.rate3").css("color","gold");
      $(".rate4,.rate5").css("color","black");
  });
  $(".rate4").hover(function(){
      $(".rate1,.rate2,.rate3,.rate4").css("color","gold");
      $(".rate5").css("color","black");
  });
  $(".rate5").hover(function(){
      $(".rate1,.rate2,.rate3,.rate4,.rate5").css("color","gold");
  });
    
  $(".rate1").click(function(){
      $(this).css("color","gold");
      $(this).attr("class","rate_active");
      $(".rate2,.rate3,.rate4,.rate5").css("color","black");
  });
  $(".rate2").click(function(){
      $(".rate1,.rate2").css("color","gold");
      $(".rate1,.rate2").attr("class","rate_active");
      $(".rate3,.rate4,.rate5").css("color","black");
  });
  $(".rate3").click(function(){
      $(".rate1,.rate2,.rate3").css("color","gold");
      $(".rate1,.rate2,.rate3").attr("class","rate_active");
      $(".rate4,.rate5").css("color","black");
  });
  $(".rate4").click(function(){
      $(".rate1,.rate2,.rate3,.rate4").css("color","gold");
      $(".rate1,.rate2,.rate3,.rate4").attr("class","rate_active");
      $(".rate5").css("color","black");
  });
  $(".rate5").click(function(){
      $(".rate1,.rate2,.rate3,.rate4,.rate5").css("color","gold");
      $(".rate1,.rate2,.rate3,.rate4,.rate5").attr("class","rate_active");
  });
    

    
  $(".rating").mouseleave(function(){
      
      $(".rate1,.rate2,.rate3,.rate4,.rate5").not(".rate_active").css("color","black");
  });  
});

 $(document).ready(function() {
    $("#lightSlider").lightSlider({
        item:1,
        loop:true,
        gallery:true,
        thumbItem:5,
        slideMargin: 0,
        auto:true
        
    }); 
  });

$(document).ready(function() {
    $(".menu_close_button").click(function(){
        $(".main_menu_for_sm ul").slideUp().css("display","none !important");
        $(".nav_button_area ul").show();
    });
    $(".nav_button").click(function(){
        $(".nav_button_area ul").hide();
        $(".main_menu_for_sm > ul").slideDown().css("display","block !important");
        
    });
    
  });

$(document).ready(function() {
    $("#comment_button").click(function(){
        var user_id=$("#user_id").val();
        var ip=$("#ip").val();
        var comment=$("#comment").val();
        var news_id=$("#news_id").val();
        
        $.post(
            "comment.php",
            {
                user_id:user_id,
                ip:ip,
                comment:comment,
                news_id:news_id
            },
            function(data,status){
                if(status=='success'){
                    $(".commentList").html(data);
                    $("#comment").val("");
                    
                } 
            }
        );
        
    });
    
    $("#comment").keypress(function(e){
        if(e.which==13){
        var user_id=$("#user_id").val();
        var ip=$("#ip").val();
        var comment=$("#comment").val();
        var news_id=$("#news_id").val();
        $.post(
            "comment.php",
            {
                user_id:user_id,
                ip:ip,
                comment:comment,
                news_id:news_id
            },
            function(data,status){
                if(status=='success'){
                    $(".commentList").html(data);
                    $("#comment").val(""); 
                } 
            }
        );
        
        }//if condition 
    }); 
  });

// popup 
$(document).ready(function(){
    $(".zoom-layer").click(function(){
        var post_title=$("#post_title").val();
        $(".popup-layer").css("transform","scale(1)");
        $.post("login_popup.php",
               {
                post_title:post_title
                }
               ,
               function(data){
            $(".popup-details").html(data);
        });
    });
    $(".popup-close").click(function(){
        $(".popup-layer").css("transform","scale(0)");
    });
    
    //colse popup
});

$(document).ready(function(){
    lightbox.option({
      'resizeDuration': 200,
      'wrapAround': true
    })
});

$(document).ready(function(){
    $("#search_content").keyup(function(){
        var search_content = $(this).val();
        var lenth= search_content.length;
            if(lenth>1){
                $(".suggesion").html('<i class="fa fa-spinner" aria-hidden="true"></i>');
                $.post("ajx_search.php",
                       {
                        search_content:search_content
                        }
                       ,
                       function(data){
                   $(".suggesion").html(data);
                });
            }
    });
    $("#search_content").keydown(function(){
        var search_content = $(this).val();
        var lenth= search_content.length;
            if(lenth<2){
                $(".suggesion").html('<i class="fa fa-spinner" aria-hidden="true"></i>');
                $(".suggesion").empty();
            }
    });

    
    //left and right side advertise
    var window_width=$(window).width();
    var container_width=$(".container").width();
    var net_width=window_width-container_width;
    var side_width=net_width/2-15;
    $(".side-ad").width(side_width+"px");
    
    $("#survey_form").submit(function(e){
      e.preventDefault();
      var radio_type=$("input[type='radio']:checked",this);
      var lenth=radio_type.length;
      if(lenth==1){
        var form_data=$(this).serialize();
        var action=$(this).attr("action");
        $.post(action,form_data,function(data){
          $(".action-message").html(data);
        });
      }
    });
    
    
});

