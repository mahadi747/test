<html>
    <head>
        <title>slider</title>
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="font-awesome-4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="css/style.css">
    </head>
    <body>
        <section>
            <div class="">
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h2>Md.Mahadi Hasan Noyon</h2>
                            </div>
                            <div class="panel-body">
                                <div class="profile-photo">
                                    <img src="images/family-member/dsc_2626.jpg" alt="">
                                </div>
                                <div class="info-section basic info">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <h3>সাধারন তথ্য</h3>
                                        </div>
                                    </div><!-- end heading row-->
                                    <div class="row">
                                        <div class="col-md-3 info-name">
                                            নাম
                                        </div>
                                        <div class="col-md-1">:</div><!-- end devider -->
                                        <div class="col-md-8">
                                            মোঃ মেহেদী হাসান নয়ন
                                        </div>
                                    </div><!-- end a part of information -->
                                    <div class="row">
                                        <div class="col-md-3 info-name">
                                            আইডি নং
                                        </div>
                                        <div class="col-md-1">:</div><!-- end devider -->
                                        <div class="col-md-8">
                                            ৪৩৭৫৮৩
                                        </div>
                                    </div><!-- end a part of information -->
                                    <div class="row">
                                        <div class="col-md-3 info-name">
                                            জন্ম তারিখ
                                        </div>
                                        <div class="col-md-1">:</div><!-- end devider -->
                                        <div class="col-md-8">
                                            ১০-০৬-১৯৯৬
                                        </div>
                                    </div><!-- end a part of information -->
                                    <div class="row">
                                        <div class="col-md-3 info-name">
                                            পিতার নাম
                                        </div>
                                        <div class="col-md-1">:</div><!-- end devider -->
                                        <div class="col-md-8">
                                            মোঃ বাবুল হোসেন
                                        </div>
                                    </div><!-- end a part of information -->
                                    <div class="row">
                                        <div class="col-md-3 info-name">
                                            মাতার নাম
                                        </div>
                                        <div class="col-md-1">:</div><!-- end devider -->
                                        <div class="col-md-8">
                                            মোঃ সাহেরা খাতুন
                                        </div>
                                    </div><!-- end a part of information -->
                                    <div class="row">
                                        <div class="col-md-3 info-name">
                                            লিঙ্গ
                                        </div>
                                        <div class="col-md-1">:</div><!-- end devider -->
                                        <div class="col-md-8">
                                            পুরুষ
                                        </div>
                                    </div><!-- end a part of information -->
                                    <div class="row">
                                        <div class="col-md-3 info-name">
                                            ধর্ম
                                        </div>
                                        <div class="col-md-1">:</div><!-- end devider -->
                                        <div class="col-md-8">
                                            পুরুষ
                                        </div>
                                    </div><!-- end a part of information -->
                                </div><!-- end section information -->
                                <div class="info-section education-info">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <h3>শিক্ষাগত যোগ্যতা</h3>
                                        </div>
                                    </div><!-- end heading row-->
                                    <div class="row qulification">
                                        <div class="col-md-3 info-name">
                                          এস এস সি
                                        </div>
                                        <div class="col-md-1">:</div><!-- end devider -->
                                        <div class="col-md-8">
                                            <div class="row">
                                                <div class="col-md-4">প্রতিষ্ঠানের নাম</div>
                                                <div class="col-md-4">পাশের শন</div>
                                                <div class="col-md-4">ফলাফল</div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">হোসেনাবাদ টেকনিক্যাল স্কুল এন্ড কলেজ</div>
                                                <div class="col-md-4">২০১২</div>
                                                <div class="col-md-4">২.২৭</div>
                                            </div>
                                        </div>
                                    </div><!-- end a part of information -->
                                    <div class="row qulification">
                                        <div class="col-md-3 info-name">
                                         এইস এস সি
                                        </div>
                                        <div class="col-md-1">:</div><!-- end devider -->
                                        <div class="col-md-8">
                                            <div class="row">
                                                <div class="col-md-4">প্রতিষ্ঠানের নাম</div>
                                                <div class="col-md-4">পাশের শন</div>
                                                <div class="col-md-4">ফলাফল</div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">হোসেনাবাদ টেকনিক্যাল স্কুল এন্ড কলেজ</div>
                                                <div class="col-md-4">২০১২</div>
                                                <div class="col-md-4">২.২৭</div>
                                            </div>
                                        </div>
                                    </div><!-- end a part of information -->
                                </div><!-- end section information -->
                                <div class="info-section contact-info">
                                    <div class="col-md-12 social">
                                        <div class="">
                                            <a href="#"><i class="fa fa-facebook"></i></a>
                                            <a href="#"><i class="fa fa-twitter"></i></a>
                                            <a href="#"><i class="fa fa-linkedin"></i></a>
                                            <a href="#"><i class="fa fa-pintarest"></i></a>
                                            <a href="#"><i class="fa fa-google-plus"></i></a>
                                        </div>
                                    </div><!-- end social contact part -->
                                    <div class="row">
                                        <div class="col-md-12">
                                            <h3>যোগাযোগের তথ্য</h3>
                                        </div>
                                    </div><!-- end heading row-->
                                    <div class="row">
                                        <div class="col-md-10 address_info">
                                            <div class="row address">
                                                <div class="col-md-11">
                                                    <div class="row">
                                                        <div class="col-md-4 info-name">
                                                            গ্রামের নাম
                                                        </div>
                                                        <div class="col-md-1">:</div><!-- end devider -->
                                                        <div class="col-md-7">
                                                            বাগোয়ান
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-4 info-name">
                                                            ডাকঘর
                                                        </div>
                                                        <div class="col-md-1">:</div><!-- end devider -->
                                                        <div class="col-md-7">
                                                            মথুরাপুর
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-4 info-name">
                                                            উপজেলা
                                                        </div>
                                                        <div class="col-md-1">:</div><!-- end devider -->
                                                        <div class="col-md-7">
                                                            দৌলতপুর
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-4 info-name">
                                                            জেলা
                                                        </div>
                                                        <div class="col-md-1">:</div><!-- end devider -->
                                                        <div class="col-md-7">
                                                            কুষ্টিয়া
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-4 info-name">
                                                            বিভাগ
                                                        </div>
                                                        <div class="col-md-1">:</div><!-- end devider -->
                                                        <div class="col-md-7">
                                                            খুলনা
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-4 info-name">
                                                            দেশ
                                                        </div>
                                                        <div class="col-md-1">:</div><!-- end devider -->
                                                        <div class="col-md-7">
                                                           বাংলাদেশ
                                                        </div>
                                                    </div>
                                                    
                                                </div><!-- info -->
                                                <div class="col-md-1 single-letter">
                                                    <p>ব র্ত মা ন</p>
                                                </div><!-- end singl latter present information -->
                                            </div><!-- present info -->
                                            <div class="row address">
                                                <div class="col-md-11">
                                                    <div class="row">
                                                        <div class="col-md-4 info-name">
                                                            গ্রামের নাম
                                                        </div>
                                                        <div class="col-md-1">:</div><!-- end devider -->
                                                        <div class="col-md-7">
                                                            বাগোয়ান
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-4 info-name">
                                                            ডাকঘর
                                                        </div>
                                                        <div class="col-md-1">:</div><!-- end devider -->
                                                        <div class="col-md-7">
                                                            মথুরাপুর
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-4 info-name">
                                                            উপজিলা/থানা
                                                        </div>
                                                        <div class="col-md-1">:</div><!-- end devider -->
                                                        <div class="col-md-7">
                                                            দৌলতপুর
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-4 info-name">
                                                            জেলা
                                                        </div>
                                                        <div class="col-md-1">:</div><!-- end devider -->
                                                        <div class="col-md-7">
                                                            কুষ্টিয়া
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-4 info-name">
                                                            বিভাগ
                                                        </div>
                                                        <div class="col-md-1">:</div><!-- end devider -->
                                                        <div class="col-md-7">
                                                            খুলনা
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-4 info-name">
                                                            দেশ
                                                        </div>
                                                        <div class="col-md-1">:</div><!-- end devider -->
                                                        <div class="col-md-7">
                                                           বাংলাদেশ
                                                        </div>
                                                    </div>
                                                    <!----------------------------------------------------------->
                                                    <div class="row">
                                                        <div class="col-md-4 info-name">
                                                            মোবাইল নং
                                                        </div>
                                                        <div class="col-md-1">:</div><!-- end devider -->
                                                        <div class="col-md-7">
                                                            ০১৭৭৯২৮২৭৪৭
                                                        </div>
                                                    </div><!-- end a part of information -->
                                                    <div class="row">
                                                        <div class="col-md-4 info-name">
                                                           ইমেইল
                                                        </div>
                                                        <div class="col-md-1">:</div><!-- end devider -->
                                                        <div class="col-md-7">
                                                            hasanmahadi889@gmail.com
                                                        </div>
                                                    </div><!-- end a part of information -->
                                                    
                                                    <!---------------------------------------------------------------->
                                                </div><!-- info -->
                                                <div class="col-md-1 single-letter">
                                                    <p>স্থা য়ী</p>
                                                </div><!-- end singl latter present information -->
                                            </div><!-- parmanent info -->
                                            
                                            
                                        </div><!-- end basic contact part -->
                                    </div><!-- end a part of information -->
                                    
                                </div><!-- end section information -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </body>
</html>
