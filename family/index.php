<?php include("include/header.php");?>
        <div class="search_bar">
            <div class="container">
                <div class="row">
                    <form method="post" action="">
                    <div class="col-md-9">
                        <div class="col-md-4">
                            <select class="form-control">
                                <option>বিভাগ</option>
                                <option>ঢাকা</option>
                                <option>খুলনা</option>
                            </select>
                            <span><i class="fa fa-map-marker" aria-hidden="true"></i></span>
                        </div>
                        <div class="col-md-4">
                            <select class="form-control">
                                <option>থানা</option>
                                <option>কুষ্টিয়া</option>
                                <option>ঢাকা</option>
                            </select>
                            <span><i class="fa fa-street-view" aria-hidden="true"></i></span>
                        </div>
                        <div class="col-md-4">
                            <select class="form-control">
                                <option>থানা</option>
                                <option>দৌলতপুর</option>
                                <option>ভেড়ামারা</option>
                            </select>
                            <span><i class="fa fa-map-pin" aria-hidden="true"></i></span>
                        </div>
                    </div><!-- search bar -->
                    <div class="col-md-1">
                        <button class="btn btn-info">খুজুন <i class="fa fa-search"></i></button>
                    </div>
                    </form>
                    <div class="col-md-2">
                        <a href="#" class="btn btn-default">যুক্ত হন</a>
                    </div><!-- END  add new button -->
                </div>
            </div>
        </div>
        <section>
            <div class="container employers">
                <div class="row">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="helper">
                                <div class="photo">
                                    <img src="images/family-member/c2876bc260bcfc60236b1c357c6887e7.jpg" alt="">
                                </div>
                                <p><span>নাম: </span>মোঃ মেহেদী হাসান নয়ন</p>
                                <p><span>এলাকা: </span> মিরপুর - ১</p>
                                <p><span>বিভাগ: </span> বাংলাদেশ</p>
                                <div class="zoom-layer">
                                    <i class="fa fa-search-plus" aria-hidden="true"></i>
                                </div>
                            </div>
                            
                        </div><!-- end single person -->
                        <div class="col-md-3">
                            <div class="helper">
                                <div class="photo">
                                    <img src="images/family-member/cd267d9afa3343b50c468b3daec610bc.jpg" alt="">
                                </div>
                                <p><span>নাম: </span>মোঃ ফরহাদ ইসলাম</p>
                                <p><span>এলাকা: </span> ধানমন্ডি</p>
                                <p><span>বিভাগ: </span> খেলাধুলা</p>
                            </div>
                        </div><!-- end single person -->
                        <div class="col-md-3">
                            <div class="helper">
                                <div class="photo">
                                    <img src="images/family-member/Makeup-Artist-Cairns-Glamour-Formal-Portrait.jpg" alt="">
                                </div>
                                <p><span>নাম: </span>খন্দকার ফারিয়া </p>
                                <p><span>এলাকা: </span> ফার্মগেট</p>
                                <p><span>বিভাগ: </span> ডিজাইন</p>
                            </div>
                        </div><!-- end single person -->
                        <div class="col-md-3">
                            <div class="helper">
                                <div class="photo">
                                    <img src="images/family-member/dsc_2626.jpg" alt="">
                                </div>
                                <p><span>নাম: </span>মোঃ রাশেদ  উদ্দিন</p>
                                <p><span>এলাকা: </span> ধানমন্ডি</p>
                                <p><span>বিভাগ: </span> টেলিভিশন</p>
                            </div>
                        </div><!-- end single person -->
                        <div class="col-md-3">
                            <div class="helper">
                                <div class="photo">
                                    <img src="images/family-member/cd267d9afa3343b50c468b3daec610bc.jpg" alt="">
                                </div>
                                <p><span>নাম: </span>মোঃ ফরহাদ ইসলাম</p>
                                <p><span>এলাকা: </span> ধানমন্ডি</p>
                                <p><span>বিভাগ: </span> খেলাধুলা</p>
                            </div>
                        </div><!-- end single person -->
                        <div class="col-md-3">
                            <div class="helper">
                                <div class="photo">
                                    <img src="images/family-member/LW3.jpg" alt="">
                                </div>
                                <p><span>নাম: </span>খন্দকার ফারিয়া </p>
                                <p><span>এলাকা: </span> ফার্মগেট</p>
                                <p><span>বিভাগ: </span> ডিজাইন</p>
                            </div>
                        </div><!-- end single person -->
                        <div class="col-md-3">
                            <div class="helper">
                                <div class="photo">
                                    <img src="images/family-member/c2876bc260bcfc60236b1c357c6887e7.jpg" alt="">
                                </div>
                                <p><span>নাম: </span>মোঃ মেহেদী হাসান নয়ন</p>
                                <p><span>এলাকা: </span> মিরপুর - ১</p>
                                <p><span>বিভাগ: </span> বাংলাদেশ</p>
                            </div>
                        </div><!-- end single person -->
                        <div class="col-md-3">
                            <div class="helper">
                                <div class="photo">
                                    <img src="images/family-member/psbaqc2016formal17.jpg" alt="">
                                </div>
                                <p><span>নাম: </span>পারভিন শিখা </p>
                                <p><span>এলাকা: </span> ফার্মগেট</p>
                                <p><span>বিভাগ: </span> ডিজাইন</p>
                            </div>
                        </div><!-- end single person -->
                    </div>
                </div>
            </div>
        </section>
        <section class="popup-layer">
            <div class="popup-content">
                <div class="popup-close-area">
                    <div class="popup-close">
                        <i class="fa fa-window-close-o" aria-hidden="true"></i>
                    </div>
                </div>
                <div class="popup-details">
                
                </div>
            </div>
        </section>
 <?php include("include/footer.php");?>       