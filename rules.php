<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<html lang="bn">
<head>

<?php include "exsit-admin/db_config.php"; ?>
<?php 
    $category_name=$_REQUEST['category']; 
    
    $menu_sql=mysql_query("select * from menu_manage where menu_name='$category_name'");
    $menu_data=mysql_fetch_assoc($menu_sql);
    $menu_id=$menu_data['id'];
    
    include 'paginate.php';

	

//////////////////////////////////////////////////////////////////////////////////////////////////////
	// this is for pagination
	//////////////////////////////////////////////////////////////////////////////////////////////////////
	// set $limit (per page) for pagination
	$limit = 2;
	
	
     //show records
    $query = mysql_query("select * from news_manage where menu_name='$menu_id' order by id desc");
    $total_rows = mysql_num_rows($query);
	
	$page;
	// prepare $_GET['page'] variable for pagination
	if(isset($_GET['page']) && (int)$_GET['page']){
		
		if($_GET['page'] > ceil($total_rows/$limit)){
			$page = 1;
		} else {
			$page = $_GET['page'];
		}
		
	} else {
		$_GET['page'] = 1;
		$page = 1;
	}
	
	$current = ($page - 1) * $limit;
	
	// current set of data to be displayed
	$current_display = ceil($total_pages/$_GET['page']);
	
?>
    
    
    
    

<?php include "head.php"; ?>
<meta name="keywords" content=""/>
<meta name="description" content="আপনি কি মোহনা সংবাদ ২৪ অনলাইনে প্রকাশিত কোনো বিষয় সম্পর্কে মন্তব্য করতে চান?"/>
<meta name="title" content="মন্তব্য প্রদানের নীতিমালা"/>
</head>

<body>
<div class="container">
<div class="box-layout">
<!--header-->
<?php include "header.php"; ?>
<!--header--> 

<!--------------menu--------------->
<?php include "main_menu.php"; ?>
<!--------------menu--------------->
<div class="row">
    <div class="col-md-8">
        <div class="row visited-location">
            <div class="col-md-2 here-now"> Here Now </div>
            <div class="col-md-10 here-now-location">
                <a href="index.php">হোম</a> <span><i class="fa fa-angle-double-right" aria-hidden="true"></i></span>
                <a href="rules.php">মন্তব্য প্রদানের নীতিমালা</a> <span><i class="fa fa-angle-double-right" aria-hidden="true"></i></span>
            </div>
        </div>
        <!-- end visited location -->
        <div class="row category-page-panel-head">
            <div class="col-md-12">
                <h1>মন্তব্য প্রদানের নীতিমালা</h1>
            </div>
        </div>
        <!-- end category panel -->
        <div class="row category-page-content">
            <div class="col-md-12">
                <p>
                    মন্তব্য ও প্রশ্নকারীর প্রতি
                    আপনি কি মোহনা সংবাদ ২৪ অনলাইনে প্রকাশিত কোনো বিষয় সম্পর্কে মন্তব্য করতে চান? মোহনা সংবাদ ২৪ সম্পাদকের কাছে কি আপনার কোনো প্রশ্ন আছে?
                    তাহলে নিম্নলিখিত বিষয়গুলো অবশ্যই খেয়াল রাখুন:
                </p>


                <p>১. বাংলাদেশের প্রচলিত আইন ভঙ্গ করে, এমন কোনো মন্তব্য বা প্রশ্ন করা থেকে বিরত থাকুন।</p>
                <p>২. কোনো লেখায় আন্তর্জাতিক কোনো আইন লঙ্ঘন করবেন না।</p>
                <p>৩. আপনার মন্তব্যে শালীনতা বজায় রাখুন। সমালোচনা করুন, কিন্তু কোনো ব্যক্তি বা প্রতিষ্ঠানের বিরুদ্ধে অশালীন, অশোভন শব্দ ব্যবহার করবেন না।</p>
                <p>৪. কারও ধর্মীয় অনুভূতিতে আঘাত লাগতে পারে, এমন মন্তব্য বা প্রশ্ন করা থেকে বিরত থাকুন।</p>
                <p>৫. কোনো ক্ষুদ্র জাতিসত্তা বা সংখ্যালঘু সম্প্রদায়ের ধর্ম, সংস্কৃতি, আচার, জীবনযাত্রা ও খাদ্যাভ্যাস কিংবা ভাষাকে অবমাননা করে এমন কোনো মন্তব্য বা প্রশ্ন করবেন না।</p>
                <p>৬. নিবন্ধনের সময় ইউজার আইডি হিসেবে কোনো অশালীন শব্দ/ ওয়েবসাইটের লিঙ্ক/ স্লোগান/ নিজস্ব সার্থে ব্যবহ্রত শব্দ বা প্রোফাইলে আপত্তিকর ছবি ব্যবহার করবেন না।</p>
                <p>৭. মন্তব্য বা প্রশ্নে কোনো ব্যক্তিগত বিজ্ঞাপন দেওয়া থেকে বিরত থাকুন।</p>
                <p>৮. এমন কোনো মন্তব্য বা প্রশ্ন করবেন না, যা বাংলাদেশে সামাজিক ও সাংস্কৃতিকভাবে অশ্লীল বলে বিবেচিত।</p>
                <p>৯. বাংলাদেশের মুক্তিযুদ্ধবিরোধী, স্বাধীনতা ও সার্বভৌমত্বের প্রতি হুমকিস্বরূপ কোনো কিছু লেখা যাবে না।</p>
                <p>১০. রোমান হরফে বাংলায় লেখা কোনো মন্তব্য প্রকাশ করা হবে না।</p>
                <p>১১. যেকোনো মন্তব্য ও প্রশ্ন সম্পাদনার এবং প্রকাশ করা ও না করার পূর্ণ অধিকার প্রথম আলো সংরক্ষণ করে।</p>
                <p>১২. এই নীতিমালা যেকোনো সময় পরিবর্তনের অধিকার সংরক্ষণ করে মোহনা সংবাদ ২৪ ।</p>
            </div>
            
        </div><!-- end news list -->
    </div>
    

   

<?php include "right_bur_news.php"; ?>





<?php include("footer.php");?>


    
