<?php
	#NAME : Md. SHAMIM HASAN
	#E-Mail : smshamimhasanbd@gmail.com
	#PHONE : +88 01747 21 63 12
	include ("db_config.php");
	include ("session.php");
	
?>
	


<!DOCTYPE html>
<html lang="en">
<head>
<?php include ("head.php"); ?>
</head>
<body class="page-body  page-fade" data-url="">

<div class="page-container">     
	
	<?php include ("sidebar_menu.php");?>

	<?php include ("top_menu.php");?>
		
		 <div id="page-wrapper">    
		 
            <div id="page-inner"> 


                <div class="row">
                    <div class="col-md-12">
                        <h1 class="page-header">
                            Employer Add <small> New Employer Add </small>                        </h1>
                  </div>
                </div>
				
			 			<ol class="breadcrumb">
                            <li>
                                <i class="fa fa-dashboard"></i>  <a href="deshbord.php">Dashboard</a>                            </li>
							
							 <li>
                                <i class="fa fa-file"></i>  <a href="employer_manage.php">Employer Manage</a>                            </li>
							
                            <li class="active"> Employer Add </li>
                        </ol>
		

                    <div class="col-md-12 col-sm-12 col-xs-12">
 
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Employer Add                            </div> 
                            <div class="panel-body">
                                <div class="table-responsive">
                                    
									
										
						
						<form class="form-horizontal" role="form" id="setting" name="post" method="post" action="employer_add_ac.php" enctype="multipart/form-data">
						
						
							 
						
							 <div class="form-group">
								<label for="text" class="col-sm-2 control-label">Employer Name : </label>
								<div class="col-sm-10">
								  <input name="em_name" type="text" class="form-control" id="text" placeholder="Enter Employer Name  " >
								</div>
								
						  </div>
						
						
							<div class="form-group">
								<label for="text" class="col-sm-2 control-label">Employer Designation : </label>
								<div class="col-sm-10">
								  <input name="em_design" type="text" class="form-control" id="text" placeholder="Enter Employer Designation" >
								</div>
								
							  </div>
							  
							 <div class="form-group">
								<label for="text" class="col-sm-2 control-label">Phone Number : </label>
								<div class="col-sm-10">
								  <input name="em_phon" type="text" class="form-control" id="text" placeholder="Enter Phone Number" >
								</div>
								
							  </div>
							  
							  <div class="form-group">
								<label for="text" class="col-sm-2 control-label"> E-mail ID : </label>
								<div class="col-sm-10">
								  <input name="em_mail" type="text" class="form-control" id="text" placeholder="Enter E-mail ID " >
								</div>
								</div>
								
								 <div class="form-group">
								<label for="text" class="col-sm-2 control-label">Facebook Link : </label>
								<div class="col-sm-10">
								  <input name="fb_lin" type="text" class="form-control" id="text" placeholder="Enter Facebook Link" >
								</div>
								
							  </div>
							  
							   <div class="form-group">
								<label for="text" class="col-sm-2 control-label">Twitter Link : </label>
								<div class="col-sm-10">
								  <input name="tw_lin" type="text" class="form-control" id="text" placeholder="Enter Twitter Link" >
								</div>
								
							  </div>
								
							 <div class="form-group">
								<label for="text" class="col-sm-2 control-label">Employer Image : </label>
								<div class="col-sm-10">
								<div style="position:relative;">
									
									<a class='' href='javascript:;'>
										Image Upload...
										<span class="btn btn-white btn-file">
					<input name="em_imgs" type="file" class="uploader" id="em_imgs"  onchange='$("#upload-file-info").html($(this).val());' size="40">
										<span class="fileinput-new">Select image</span>
									</a>
									
									&nbsp;
									<span class='label label-info' id="upload-file-info"></span><p style="color:#FF0000; font-style:italic;"> image size 240/240</p>
								</div>
							  </div>
							  </div>
							  
					  
							 
						  <div class="form-group">
							<div class="col-sm-offset-2 col-sm-10">
							  <button type="submit" class="btn btn-success btn-lg">Save</button>
							</div>
						  </div>
												  
						  
						 
						</form>
						
									
									
                                </div>
                            </div>
                        </div>
			
            	</div>       
<footer class="main">
			
			<?php include ("footer.php");?>
		
  </footer>
</div>
</div>
		


</div>

	
<?php include ("all_script_end.php");?>
</body>
</html>