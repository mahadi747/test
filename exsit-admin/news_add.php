<?php
	#NAME : Md. SHAMIM HASAN
	#E-Mail : smshamimhasanbd@gmail.com
	#PHONE : +88 01747 21 63 12
	include ("db_config.php");
	include ("session.php");
	
?>
	


<!DOCTYPE html>
<html lang="bn">
<head>
<?php include ("head.php"); ?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"></head>
<body class="page-body  page-fade" data-url="">

<div class="page-container"> 
	
	<?php include ("sidebar_menu.php");?>

	<?php include ("top_menu.php");?>
		
		 <div id="page-wrapper"> 
		 
            <div id="page-inner">


                <div class="row">
                    <div class="col-md-12">
                        <h1 class="page-header">
                           News Add <small> New News Add </small>                        </h1>
                  </div>
                </div>
				
			 			<ol class="breadcrumb">
                            <li>
                                <i class="fa fa-dashboard"></i>  <a href="deshbord.php">Dashboard</a>                            </li>
							
							 <li>
                                <i class="fa fa-file"></i>  <a href="news_manage.php">News Manage</a>                            </li>
							
                            <li class="active"> সংবাদ নিবন্ধন  </li>
                        </ol>
		

                    <div class="col-md-12 col-sm-12 col-xs-12">

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                সংবাদ নিবন্ধন <span class="pull-right link_button">Link Title</span>
                            </div> 
                            <div class="panel-body">
                                <div class="table-responsive">
                                    
							<form class="form-horizontal" role="form" id="setting" name="post" method="post" action="news_add_ac.php" enctype="multipart/form-data">
						
							<div class="form-group">
								<label for="text" class="col-sm-2 control-label"> শিরোনাম  : </label>
								<div class="col-sm-10">
								  <input name="title_news" type="text" class="form-control" id="title_news" placeholder="শিরোনাম দিন " onkeyup="title_maker()" maxlength="60">
						 		</div>
								
						  </div>
						  <div class="form-group" id="link_title_parent">
								<label for="text" class="col-sm-2 control-label"> title for url  : </label>
								<div class="col-sm-10">
								  <input name="link_title" type="text" class="form-control" id="link_title" value="" >
						 		</div>
								
						  </div>
						  <div class="form-group">
								<label for="text" class="col-sm-2 control-label"> বিভাগ   : </label>
								<div class="col-sm-10">
									<select name="menu_name" class="form-control" id="menu_name" onChange="sub_view_fun();">
										  <option value="">বিভাগ নির্বাচন করুন </option>
										  <?php 
											$tag = mysql_query("select * from menu_manage order by id asc");
											while($tut = mysql_fetch_array($tag)){
										  ?>
										  <option value="<?php echo $tut["id"]; ?>"><?php echo $tut["menu_name"]; ?></option>
										  <?php
											}
											?>
								  </select>
							  </div>								
						  </div>
						  
						   <div class="form-group">
								<label for="text" class="col-sm-2 control-label">সহ বিভাগ   : </label>
								<div class="col-sm-10">
									
									<div id="sub_item_view">
									
									
									 <select name="" class="form-control" id="" >
									
										<option value="">সহ বিভাগ নির্বাচন</option>
	
									</select>
								
								</div>
									
									
							  </div>								
						  </div>
						  
						  
						  <div class="form-group">
								<label for="text" class="col-sm-2 control-label">বিস্তারিত : </label>
								<div class="col-sm-10">
								  <textarea name="descriptions" class="form-control ckeditor" id="descriptions"> </textarea>
								</div>
								
						  </div>	  
							<div class="form-group">
								<label for="text" class="col-sm-2 control-label">সংক্ষিপ্ত : </label>
								<div class="col-sm-10">
								  <textarea name="short_desc" class="form-control" placeholder="সংক্ষিপ্ত বিবরণ লিখুন" maxlength="250"></textarea>
								</div>
								
						  </div>	  
							  
						
						   <div class="form-group">
							<label for="text" class="col-sm-2 control-label">ছবি : </label>
							<div class="col-sm-10">
							  	
								<div style="position:relative;">
								
									<a class='' href='javascript:;'> 
										ছবি আপলোড করুন...
										<span class="btn btn-white btn-file">
					<input name="news_img" type="file" class="uploader" id="news_img"  onchange='$("#upload-file-info").html($(this).val());' size="40">
										<span class="fileinput-new">সিলেক্ট করুন </span>
									</a>
									
									</div>
								</div>
						  </div>
						
						<div class="form-group">
								<label for="text" class="col-sm-2 control-label">ভিডিও লিঙ্ক  : </label>
								<div class="col-sm-10">
								  <input name="video_link" type="text" class="form-control" id="video_link" placeholder="ভিডিও লিঙ্ক দিন  " >
								</div>
								
						  </div>  
						  <div class="form-group">
								<label for="text" class="col-sm-2 control-label">Meta Description  : </label>
								<div class="col-sm-10">
                                    <textarea name="meta_description"  class="form-control"  placeholder=" Meta Description " ></textarea>
								</div>
								
						  </div>  
						  <div class="form-group">
								<label for="text" class="col-sm-2 control-label">Meta Keyword  : </label>
								<div class="col-sm-10">
                                    <textarea name="meta_keyword"  class="form-control"  placeholder=" Meta Keywords " ></textarea>
								</div>
								
						  </div>  
						  <div class="form-group">
								<label for="text" class="col-sm-2 control-label">Publish Date  : </label>
								<div class="col-sm-10">
                                    <input name="date"  class="form-control datepicker" value="<?php echo date("m/d/Y");?>">
								</div>
								
						  </div>  
						  
						  <div class="form-group">
								<label for="text" class="col-sm-2 control-label">সাংবাদিকের নাম : </label>
								<div class="col-sm-10">
                                    <select name="reporter_id" class="form-control">
                                        <?php 
                                            $sql=mysql_query("select * from journalist");
                                            while($data=mysql_fetch_assoc($sql))
                                            {?>
                                        <option value="<?php echo $data['id'];?>"><?php echo $data['name'];?></option>
                                        <?php } ?>
                                    </select>    
								</div>
						  </div>  
				            <div class="form-group">
								<label for="text" class="col-sm-2 control-label">প্রচ্ছদ অনুমতি : </label>
								<div class="col-sm-10">
                                    <select name="home_page_permission" class="form-control">
                                        <option value="0">না</option>
                                        <option value="1">হ্যা</option>
                                    </select>    
								</div>
						  </div>  
							 
						  <div class="form-group">
							<div class="col-sm-offset-2 col-sm-10"/>
							  <button type="submit" class="btn btn-success btn-lg">Save</button>
							</div>
						  </div>
						</form>
						
									
									
                                </div>
                            </div>
                        </div>

                    </div>
                
				<!-- /. BODY CONTENT END  -->
				
				
<footer class="main">
			
			<?php include ("footer.php");?>
		
  </footer>
</div>

		




	
<?php include ("all_script_end.php");?>
             
<!-- Imported scripts on this page -->
<script>
    $(document).ready(function(){
       $("#link_title_parent").hide(); 
        $(".link_button").click(function(){
            $("#link_title_parent").slideToggle(); 
        });
    });
    
</script>
 <script src="assets/js/wysihtml5/bootstrap-wysihtml5.js"></script>
 <script src="assets/js/ckeditor/ckeditor.js"></script>
 <script src="assets/js/ckeditor/adapters/jquery.js"></script>
 <script src="assets/js/uikit/js/uikit.min.js"></script>
 <script src="assets/js/codemirror/lib/codemirror.js"></script>
 <script src="assets/js/marked.js"></script>
 <script src="assets/js/uikit/addons/js/markdownarea.min.js"></script>
 <script src="assets/js/codemirror/mode/markdown/markdown.js"></script>
 <script src="assets/js/codemirror/addon/mode/overlay.js"></script>
 <script src="assets/js/codemirror/mode/xml/xml.js"></script>
 <script src="assets/js/codemirror/mode/gfm/gfm.js"></script>
 <script src="assets/js/icheck/icheck.min.js"></script>
<script>
    function title_maker(){
        var title=document.getElementById("title_news").value;
        var clr_extra_space=title.replace(/\s+/g,"-");
        document.getElementById("link_title").value=clr_extra_space;
    }
    
    
</script>
</body>
</html>
 
 

