<?php
	#NAME : Md. SHAMIM HASAN
	#E-Mail : smshamimhasanbd@gmail.com
	#PHONE : +88 01747 21 63 12
	include ("db_config.php");
	include ("session.php");
	
?>
	


<!DOCTYPE html>
<html lang="bn">
<head>
<?php include ("head.php"); ?>
</head>
<body class="page-body  page-fade" data-url="">

<div class="page-container"> 
	
	<?php include ("sidebar_menu.php");?>

	<?php include ("top_menu.php");?>
		
		 <div id="page-wrapper">
		 
            <div id="page-inner">


                <div class="row">
                    <div class="col-md-12">
                        <h1 class="page-header">
                            Menu Add <small> New Menu Add </small>                        </h1>
                  </div>
                </div>
				
			 			<ol class="breadcrumb">
                            <li>
                                <i class="fa fa-dashboard"></i>  <a href="deshbord.php">Dashboard</a>                            </li>
							
							 <li>
                                <i class="fa fa-file"></i>  <a href="menu_manage.php">Menu Manage</a>                            </li>
							
                            <li class="active">Menu Add </li>
                        </ol>
		

                    <div class="col-md-12 col-sm-12 col-xs-12">

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Menu Add
                            </div> 
                            <div class="panel-body">
                                <div class="table-responsive">
                                    
									
										
						
						<form class="form-horizontal" role="form" id="setting" name="post" method="post" action="menu_add_ac.php" enctype="multipart/form-data">
						
							<div class="form-group">
								<label for="text" class="col-sm-2 control-label"> Menu Item  : </label>
								<div class="col-sm-10">
								  <input name="menu_name" type="text" class="form-control" id="menu_name" placeholder="Enter Menu Item " >
								</div>
								
						  </div>
							  
					  
							 
						  <div class="form-group">
							<div class="col-sm-offset-2 col-sm-10">
							  <button type="submit" class="btn btn-success btn-lg">Save</button>
							</div>
						  </div>
												  
						  
						 
						</form>
						
									
									
                                </div>
                            </div>
                        </div>

                    </div>
                
				<!-- /. BODY CONTENT END  -->
				
				
<footer class="main">
			
			<?php include ("footer.php");?>
		
  </footer>
</div>

		




	
<?php include ("all_script_end.php");?>
</body>
</html>