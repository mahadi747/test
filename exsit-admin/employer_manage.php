<?php
	#NAME : Md. SHAMIM HASAN
	#E-Mail : smshamimhasanbd@gmail.com
	#PHONE : +88 01747 21 63 12
	include ("db_config.php");
	include ("session.php");
	

include 'paginate.php';

	

//////////////////////////////////////////////////////////////////////////////////////////////////////
	// this is for pagination
	//////////////////////////////////////////////////////////////////////////////////////////////////////
	// set $limit (per page) for pagination
	$limit = 10;
	
	
						 //show records
           			 	$query = mysql_query("select * from employer_manage order by id desc");
						$total_rows = mysql_num_rows($query);
	
	$page;
	// prepare $_GET['page'] variable for pagination
	if(isset($_GET['page']) && (int)$_GET['page']){
		
		if($_GET['page'] > ceil($total_rows/$limit)){
			$page = 1;
		} else {
			$page = $_GET['page'];
		}
		
	} else {
		$_GET['page'] = 1;
		$page = 1;
	}
	
	

	$current = ($page - 1) * $limit;
	
	// current set of data to be displayed
	$current_display = ceil($total_pages/$_GET['page']);
	
?>
	


<!DOCTYPE html>
<html lang="en">
<head>
<?php include ("head.php"); ?>


<style type="text/css">
<!--
.style3 {font-size: 16px; color: #000000; }
.style6 {font-size: 14px; color: #000000; }
-->
</style>
</head>
<body class="page-body  page-fade" data-url="">

<div class="page-container"> <!-- add class "sidebar-collapsed" to close sidebar by default, "chat-visible" to make chat appear always -->
	
	<?php include ("sidebar_menu.php");?>

	<?php include ("top_menu.php");?>
		
		<div id="page-wrapper">
            <div id="page-inner">


                <div class="row">
                    <div class="col-md-12">
                        <h1 class="page-header">
                        Employer Manage</h1>
                    </div>
                </div>
				
			 			<ol class="breadcrumb">
                            <li>
                                <i class="fa fa-dashboard"></i><a href="deshbord.php">Dashboard</a>                            </li>
                            <li class="active">
                                <i class="fa fa-file"></i> Employer Manage </li>
                        </ol>
					
					
					 <!-- /.row -->
				
				<div class="row">
                    <div class="col-md-12" style="background:#FFFFFF;">
                        <h2 class="page-header header" align="center">Manage</h2>
						
						<div class="col-md-3" align="center">
						&nbsp;
						</div>
						<div class="col-md-6" align="center">
						
						<a class="btn btn-success btn-lg btn-block" href="employer_add.php">
						<span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
						Add New
						</a>
						
						</div>
						
						<div class="col-md-3" align="center">
						&nbsp;
						</div>
						
				  </div>
				</div>
				
					
					<div class="row">
                    <div class="col-md-12" style="background:#FFFFFF;">
                        <h2 class="page-header header" align="center"><span class="page-header">Employer</span> List</h2>
						
				
												
                        <div class="table-responsive page-header">
						
						
                            <table class="table table-striped table-hover table-condensed">
                                <thead>
                                    <tr>
                                        <th width="5%"><span class="style3">Sl</span></th>
                                        <th width="10%"><span class="style3">Name </span></th>
										<th width="15%"><span class="style3">Designation </span></th>
										<th width="15%"><span class="style3">Phone </span></th>
										<th width="25%"><span class="style3">E-mail</span></th>
										<th width="15%"><span class="style3">Picture</span></th>
										
										
                                        <th width="30%" style="text-align:center;"><span class="style3">Action</span></th>
                                    </tr>
                                </thead>
								
				
																
                                <tbody>
								<?php 
									$tut1 = mysql_query("select * from employer_manage order by id desc LIMIT $current, $limit");
									while($tat1 = mysql_fetch_array($tut1)){
									$del_id = $tat1["id"];
									$em_name = $tat1["em_name"]; 
									$table_name = 'employer_manage';
									
										
								?>
                                    <tr>
                                        <td><span class="style6"><?php echo $tat1["id"]; ?></span></td>
                                        <td><span class="style6"><?php echo $tat1["em_name"]; ?></span></td>
										 <td><span class="style6"><?php echo $tat1["em_design"]; ?></span></td>
										 <td><span class="style6"><?php echo $tat1["em_phon"]; ?></span></td>
										 <td><span class="style6"><?php echo $tat1["em_mail"]; ?></span></td>
										 <td> <img src="em_imgs/<?php echo $tat1["em_imgs"]; ?>" width="100" height="90" /> </td>
										 
										 
                                        <td align="center" valign="middle">
											<!--<a href="customer_detils.php?view_id=<?php echo $del_id; ?>" class="btn btn-info" >
										<span class="glyphicon glyphicon-file" data-toggle="tooltip" data-placement="top" 
										data-original-title="View Details" title="View Details"></span>										
											</a>-->
									
											<a href="employer_edit.php?edit_id=<?php echo $del_id; ?>" class="btn btn-warning">
										<span class="glyphicon glyphicon-edit" data-toggle="tooltip" data-placement="top" 
										data-original-title="Edit" title="Edit"></span>											</a>
										
											<a href="javascript:;" onClick="delete_function_con('<?php echo $del_id; ?>', '<?php echo $table_name; ?>');" class="btn btn-danger">
										<span class="glyphicon glyphicon-trash" data-toggle="tooltip" data-placement="top" 
										data-original-title="Delete" title="Delete"></span>											</a>										</td>
                                    </tr>
									
									<?php
										}
									?>
                                </tbody>
                          </table>
                        </div>
                    </div>

                    </div>
		
		<div class="row">
					<div class="col-md-2" align="center">&nbsp;</div>
					<div class="col-md-8 " align="center" style=" background:#FFFFFF;">
                    <nav>
					  <ul class="pagination">
					  
						<?php
                        //pagination
                         echo paginate($total_rows, $_SERVER['PHP_SELF'] , $limit, $_GET['page'])
               			 ?>
			
					  </ul>
					</nav>
					</div>
					<div class="col-md-2" align="center">&nbsp;</div>
			  </div>	
		
		
		</div>
		<!-- Footer -->
<footer class="main">
			
			<?php include ("footer.php");?>
		
  </footer>
</div>

	</div>	




	
<?php include ("all_script_end.php");?>
</body>
</html>