<?php
	#NAME : Md. SHAMIM HASAN
	#E-Mail : smshamimhasanbd@gmail.com
	#PHONE : +88 01747 21 63 12
	include ("db_config.php"); 
?>



<!DOCTYPE html>
<html lang="en">
<head>
	<?php include ("head.php"); ?>

<style type="text/css">

  @import url(//fonts.googleapis.com/css?family=Open+Sans:400,700,800,300);
  *, *:before, *:after {
    box-sizing: border-box;
  }

  .group:after {
    content: "";
    display: table;
    clear: both;
  }

  html {
    position: absolute;
    z-index: 1;
    height: 100%;
    width: 100%;
    overflow: hidden;
  }
  html:before {
    content: '';
    display: block;
    position: absolute;
    top: 0;
    bottom: 0;
    left: 0;
    right: 0;
    width: 100%;
    height: 100%;
    z-index: -1;
    background: #fff url(logo/bc_img.jpg) top center fixed no-repeat;
    background-size: cover;
    -webkit-filter: blur(2px);
            filter: blur(2px);
    -webkit-transform: scale(1.1);
            transform: scale(1.1);
  }

  body {
    position: relative;
    overflow: auto;
    margin: 0 auto;
    padding: 0;
    width: 100%;
    height: 100%;
    background: #E2E2E2;
    color: #333;
    font-size: 100%;
    font-family: 'Open Sans','Helvetica Neue',Helvetica, sans-serif;
    line-height: 1.45;
  }

  a {
    text-decoration: none;
    color: #34495E;
    -webkit-transition: all 150ms ease-in-out;
    transition: all 150ms ease-in-out;
  }
  a:hover {
    color: #253544;
  }

  header {
    margin: 0;
    padding: 1rem 0;
    background: rgba(52, 73, 94, 0.8);
  }
  header .logo {
    display: block;
    margin: 0 1rem;
    padding: 0;
    text-align: center;
  }
  header .logo a {
    display: block;
    font-size: 1.3rem;
    color: #fff;
  }
  header p {
    display: block;
    margin: 0;
    padding: 0;
    text-align: center;
    font-size: 0.9rem;
    color: #ccc;
  }

  .container {
    display: block;
    position: relative;
    margin: 0 auto;
    width: 90%;
    max-width: 900px;
  }

  main {
    margin: 3rem 0;
    padding: 0;
  }

  article {
    border-radius: 2px;
    background: rgba(236, 240, 241, 0.9);
    padding: .5rem 1.5rem;
  }
  article h2 {
    font-size: 1.1rem;
    letter-spacing: -1px;
    margin: 1rem 0;
    padding: 0;
  }
  article p {
    padding: 0;
    margin: 1rem 0;
  }
  article code {
    display: inline-block;
    font-size: .8em;
    font-weight: normal;
    background: rgba(52, 73, 94, 0.2);
    border-radius: 3px;
    padding: 2px 3px 1px;
  }

  h1, h2, h3, h4, h5, h6 {
    margin: 0;
    padding: 0;
  }
  .login {
    background: transparent url(logo/login_back.png) center center no-repeat;
    width: 554px;
    height: 557px;
    padding: 100px 50px;
    margin: 100px auto 0 auto;
    text-align: center;
  }
  .login-inner {
    margin: 30px;
    background: rgba(0, 0, 0, 0.6);
    padding: 25px;
    box-shadow: 0 0 100px #000000;
  }
  .login h2 {
    color: #e0e0e0;
    text-shadow: 1px 1px 3px rgb(2, 2, 2);
    font-weight: 10;
    margin-bottom: 10px;
    padding-bottom: 10px;
    border-bottom: 1px solid #b9b9b9;
    font-weight: 100;
  }
  .login input {
    border: none;
    width: 100%;
    background-color: transparent;
    padding: 10px;
    border-bottom: 1px solid #717171;
    margin-bottom: 10px;
    color: white;
    text-shadow: 1px 1px 2px rgb(2, 2, 2);
    font-weight: 100;
  }
  .login button {
    background: #007d05;
    background-image: -webkit-linear-gradient(top, #007d05, #006514);
    background-image: -moz-linear-gradient(top, #007d05, #006514);
    background-image: -ms-linear-gradient(top, #007d05, #006514);
    background-image: -o-linear-gradient(top, #007d05, #006514);
    background-image: linear-gradient(to bottom, #007d05, #006514);
    padding: 10px;
    font-size: 20px;
    color: #a8ecae;
    width: 100%;
    border: none;
    border: 1px solid #006514;
    font-weight: 100;
    border-bottom: 3px solid #014c10;
  }
  

</style>


</head>
<body marginwidth="0" marginheight="0">
  <div class="login">
    <div class="login-inner">
      <h2>Authentication</h2>


<form id="login" name="login" method="post" action="login_ac.php">
<input type="text" name="user" placeholder="Enter Username"><br>
<input type="password" name="password" placeholder="Enter Password"><br>
<input type="submit" value="Submit"></form>

	
	<p>&nbsp;
	</p>
    </div>
  </div>


</body>
</html>