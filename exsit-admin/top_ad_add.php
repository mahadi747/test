<?php
	#NAME : Md. SHAMIM HASAN
	#E-Mail : smshamimhasanbd@gmail.com
	#PHONE : +88 01747 21 63 12
	include ("db_config.php");
	include ("session.php");
	
?>
	


<!DOCTYPE html>
<html lang="bn">
<head>
<?php include ("head.php"); ?>
</head>
<body class="page-body  page-fade" data-url="">

<div class="page-container"> 
	
	<?php include ("sidebar_menu.php");?>

	<?php include ("top_menu.php");?>
		
		 <div id="page-wrapper">
		 
            <div id="page-inner">


                <div class="row">
                    <div class="col-md-12">
                        <h1 class="page-header">
                            Top Ad Add <small> New Top Ad Add </small>                        </h1>
                  </div>
                </div>
				
			 			<ol class="breadcrumb">
                            <li>
                                <i class="fa fa-dashboard"></i>  <a href="deshbord.php">Dashboard</a>                            </li>
							
							 <li>
                                <i class="fa fa-file"></i>  <a href="top_ad_manage.php">Top Ad Manage</a>                            </li>
							
                            <li class="active">Top Ad Add </li>
                        </ol>
		

                    <div class="col-md-12 col-sm-12 col-xs-12">

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Top Ad Add
                            </div> 
                            <div class="panel-body">
                                <div class="table-responsive">
                                    
									
										
						
						<form class="form-horizontal" role="form" id="setting" name="post" method="post" action="top_ad_add_ac.php" enctype="multipart/form-data">
						
							<div class="form-group">
								<label for="text" class="col-sm-2 control-label"> Top Ad Name  : </label>
								<div class="col-sm-10">
								  <input name="top_ad_name" type="text" class="form-control" id="top_ad_name" placeholder="Enter Top Ad Name " >
								</div>
								
						  </div>
						  
						  <div class="form-group">
								<label for="text" class="col-sm-2 control-label"> Top Ad Code  : </label>
								<div class="col-sm-10">
                                    <textarea name="top_ad_link" type="text" class="form-control" id="top_ad_link" placeholder="Enter Top Ad Code " ></textarea>
								</div>
								
						  </div>
							  
							  
						

					  
							 
						  <div class="form-group">
							<div class="col-sm-offset-2 col-sm-10">
							  <button type="submit" class="btn btn-success btn-lg">Save</button>
							</div>
						  </div>
												  
						  
						 
						</form>
						
									
									
                                </div>
                            </div>
                        </div>

                    </div>
                
				<!-- /. BODY CONTENT END  -->
				
				
<footer class="main">
			
			<?php include ("footer.php");?>
		
  </footer>
</div>

		




	
<?php include ("all_script_end.php");?>
</body>
</html>