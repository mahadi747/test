<?php
	#NAME : Md. SHAMIM HASAN
	#E-Mail : smshamimhasanbd@gmail.com
	#PHONE : +88 01747 21 63 12
	include ("db_config.php");
	include ("session.php");
	
?>
	


<!DOCTYPE html>
<html lang="bn">
<head>
<?php include ("head.php"); ?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"></head>
<body class="page-body  page-fade" data-url="">

<div class="page-container"> 
	
	<?php include ("sidebar_menu.php");?>

	<?php include ("top_menu.php");?>
		
		 <div id="page-wrapper"> 
		 
            <div id="page-inner">


                <div class="row">
                    <div class="col-md-12">
                        <h1 class="page-header">
                           Servay News Add <small> New Servay News Add </small>                        </h1>
                  </div>
                </div>
				
			 			<ol class="breadcrumb">
                            <li>
                                <i class="fa fa-dashboard"></i>  <a href="deshbord.php">Dashboard</a>                            </li>
							
							 <li>
                                <i class="fa fa-file"></i>  <a href="servay.php"> Servay News Manage</a>                            </li>
							
                            <li class="active"> সংবাদ নিবন্ধন  </li>
                        </ol>
		

                    <div class="col-md-12 col-sm-12 col-xs-12">

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                সংবাদ নিবন্ধন <span class="pull-right link_button">Link Title</span>
                            </div> 
                            <div class="panel-body">
                                <div class="table-responsive">
                                    
							<form class="form-horizontal" role="form" id="setting" name="post" method="post" action="servay_news_add_ac.php">
						
						  <div class="form-group">
								<label for="text" class="col-sm-2 control-label">বিস্তারিত : </label>
								<div class="col-sm-10">
								  <textarea name="descriptions" class="form-control ckeditor" id="descriptions"> </textarea>
								</div>
								
						  </div>	  
						
						  <div class="form-group">
								<label for="text" class="col-sm-2 control-label">Meta Description  : </label>
								<div class="col-sm-10">
                                    <textarea name="meta_description"  class="form-control"  placeholder=" Meta Description " ></textarea>
								</div>
								
						  </div>  
						  <div class="form-group">
								<label for="text" class="col-sm-2 control-label">Meta Keyword  : </label>
								<div class="col-sm-10">
                                    <textarea name="meta_keyword"  class="form-control"  placeholder=" Meta Keywords " ></textarea>
								</div>
								
						  </div>  
						  <div class="form-group">
								<label for="text" class="col-sm-2 control-label">Publish Date  : </label>
								<div class="col-sm-10">
                                    <input name="date"  class="form-control datepicker" value="<?php echo date("m/d/Y");?>">
								</div>
								
						  </div> 
						   
						  <div class="form-group">
							<div class="col-sm-offset-2 col-sm-10"/>
							  <button type="submit" class="btn btn-success btn-lg">Save</button>
							</div>
						  </div>
						</form>
						
									
									
                                </div>
                            </div>
                        </div>

                    </div>
                
				<!-- /. BODY CONTENT END  -->
				
				
<footer class="main">
			
			<?php include ("footer.php");?>
		
  </footer>
</div>

		




	
<?php include ("all_script_end.php");?>
             
<!-- Imported scripts on this page -->
 <script src="assets/js/wysihtml5/bootstrap-wysihtml5.js"></script>
 <script src="assets/js/ckeditor/ckeditor.js"></script>
 <script src="assets/js/ckeditor/adapters/jquery.js"></script>
 <script src="assets/js/uikit/js/uikit.min.js"></script>
 <script src="assets/js/codemirror/lib/codemirror.js"></script>
 <script src="assets/js/marked.js"></script>
 <script src="assets/js/uikit/addons/js/markdownarea.min.js"></script>
 <script src="assets/js/codemirror/mode/markdown/markdown.js"></script>
 <script src="assets/js/codemirror/addon/mode/overlay.js"></script>
 <script src="assets/js/codemirror/mode/xml/xml.js"></script>
 <script src="assets/js/codemirror/mode/gfm/gfm.js"></script>
 <script src="assets/js/icheck/icheck.min.js"></script>
<script>
    function title_maker(){
        var title=document.getElementById("title_news").value;
        var clr_extra_space=title.replace(/\s+/g,"-");
        document.getElementById("link_title").value=clr_extra_space;
    }
    $(document).ready(function(){
       $("#link_title_parent").hide(); 
        $(".link_button").click(function(){
            $("#link_title_parent").slideToggle(); 
        });
    });
    
</script>
</body>
</html>
 
 

