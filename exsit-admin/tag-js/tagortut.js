// districs view SHOW
function sub_view_fun(menu_name)  // 1
{	
	
	var menu_name = document.getElementById("menu_name").value;
	
	var xmlRequest = GetXmlHttpObject();
	if (xmlRequest == null)
	return;
	
	var url = "sub_view_page.php?menu_name="+menu_name;
	
	var browser=navigator.appName;
if (browser=="Microsoft Internet Explorer")
	{
		xmlRequest.open("POST",url, true);
	}
else
	{
		xmlRequest.open("GET",url, true);
	}
xmlRequest.setRequestHeader("Content-Type", "application/x-www-formurlencoded");
xmlRequest.onreadystatechange =function()
	{
		if(xmlRequest.readyState==4)
			{							
				HandleAjaxResponse_sub_view_fun(xmlRequest); // 2
			}
	};
xmlRequest.send(null);			
return false;
}

function HandleAjaxResponse_sub_view_fun(xmlRequest) //3
{
	var xmlT=xmlRequest.responseText;
	//alert(xmlT);
	document.getElementById("sub_item_view").innerHTML=xmlT;		
return false;
}

// End districs_view


// CAPITAL HOLDER LIST VIEW
function sales_product_add_sales_ajax() // Load Student academic information
{	
	
	var item_code = document.getElementById("item_code").value;

	var xmlRequest = GetXmlHttpObject();
if (xmlRequest == null)
	return;
	
	var url = "sales_product_add_ajax.php?item_code="+item_code;

	var browser=navigator.appName;
if (browser=="Microsoft Internet Explorer")
	{
		xmlRequest.open("POST",url, true);
	}
else
	{
		xmlRequest.open("GET",url, true);
	}
xmlRequest.setRequestHeader("Content-Type", "application/x-www-formurlencoded");
xmlRequest.onreadystatechange =function()
	{
		if(xmlRequest.readyState==4)
			{							
				HandleAjaxResponse_sales_product_add_sales_ajax(xmlRequest);
			}
	};
xmlRequest.send(null);			
return false;
}

function HandleAjaxResponse_sales_product_add_sales_ajax(xmlRequest)
{
	var xmlT=xmlRequest.responseText;
	document.getElementById("product_view").innerHTML=xmlT;		
return false;
}




// CAPITAL HOLDER LIST VIEW
function sales_product_add_to_cart() // Load Student academic information
{	
	
	var in_no = document.getElementById("in_no").value;
	var item_code = document.getElementById("item_code").value;
	var item_name = document.getElementById("item_name").value;
	var sell_price = document.getElementById("sell_price").value;
	var qty = document.getElementById("qty").value;
	
	var xmlRequest = GetXmlHttpObject();
	if (xmlRequest == null)
	return;
	
	var url = "sales_product_add_to_cart_ac.php?in_no="+in_no+"&item_code="+item_code+"&item_name="+item_name+"&sell_price="+sell_price+"&qty="+qty;
	
	var browser=navigator.appName;
if (browser=="Microsoft Internet Explorer")
	{
		xmlRequest.open("POST",url, true);
	}
else
	{
		xmlRequest.open("GET",url, true);
	}
xmlRequest.setRequestHeader("Content-Type", "application/x-www-formurlencoded");
xmlRequest.onreadystatechange =function()
	{
		if(xmlRequest.readyState==4)
			{							
				HandleAjaxResponse_sales_product_add_to_cart(xmlRequest);
			}
	};
xmlRequest.send(null);			
return false;
}

function HandleAjaxResponse_sales_product_add_to_cart(xmlRequest)
{
	var xmlT=xmlRequest.responseText;
	document.getElementById("view_fin_order").innerHTML=xmlT;	
	sales_ajax_add_cart_reset();
return false;
}

//BANK ENTRY ADD FORM RESET
function sales_ajax_add_cart_reset()
{	
	document.getElementById("item_code").value="";
	document.getElementById("item_name").value="";
	document.getElementById("sell_price").value="";
	document.getElementById("qty").value="";

}




function discount_add_func(totalinc_amount) 
{	

	var Discount_Amt = document.getElementById("Discount_Amt").value;
	var xmlRequest = GetXmlHttpObject();
	if (xmlRequest == null)
    return;			
	
		var url="Discount_Amt_view.php?totalinc_amount="+totalinc_amount+"&Discount_Amt="+Discount_Amt; 
    	var browser=navigator.appName;
		if (browser=="Microsoft Internet Explorer")
		{
			xmlRequest.open("POST",url, true);
		}
		else
		{
			xmlRequest.open("GET",url, true);
		}
		
		xmlRequest.setRequestHeader("Content-Type", "application/x-www-formurlencoded");
		xmlRequest.onreadystatechange =function()
		{
			if(xmlRequest.readyState==4)
			{
				HandleAjaxResponse_discount_add_func(xmlRequest);
			}
		};
			xmlRequest.send(null);
			return false; 
} 
function HandleAjaxResponse_discount_add_func(xmlRequest)
{
	var xmlT=xmlRequest.responseText;
	document.getElementById("in_tot_amt_view").innerHTML=xmlT;
	return false;
}




// CAPITAL HOLDER LIST VIEW
function sales_add_ac() // Load Student academic information
{	
	
	var date = document.getElementById("date").value;
	var in_no = document.getElementById("in_no").value;
	var cus_name = document.getElementById("cus_name").value;
	var Discount_Amt = document.getElementById("Discount_Amt").value;
	var pay_ment_type = document.getElementById("pay_ment_type").value;
	var sales_person = document.getElementById("sales_person").value;
	var delivery_person = document.getElementById("delivery_person").value;
	
	var xmlRequest = GetXmlHttpObject();
	if (xmlRequest == null)
	return;
	
	var url = "sales_manage_add_ac.php?date="+date+"&in_no="+in_no+"&cus_name="+cus_name+"&Discount_Amt="+Discount_Amt+"&pay_ment_type="+pay_ment_type+"&sales_person="+sales_person+"&delivery_person="+delivery_person;
	
	var browser=navigator.appName;
if (browser=="Microsoft Internet Explorer")
	{
		xmlRequest.open("POST",url, true);
	}
else
	{
		xmlRequest.open("GET",url, true);
	}
xmlRequest.setRequestHeader("Content-Type", "application/x-www-formurlencoded");
xmlRequest.onreadystatechange =function()
	{
		if(xmlRequest.readyState==4)
			{							
				HandleAjaxResponse_sales_add_ac(xmlRequest);
			}
	};
xmlRequest.send(null);			
return false;
}

function HandleAjaxResponse_sales_add_ac(xmlRequest)
{
	var xmlT=xmlRequest.responseText;
	location.replace("sales_invoice_print.php");
return false;
}


// CAPITAL HOLDER LIST VIEW
function sales_cancell_ac_fun() // Load Student academic information
{	
	
	var in_no = document.getElementById("in_no").value;

	var xmlRequest = GetXmlHttpObject();
if (xmlRequest == null)
	return;
	
	var url = "sales_cancell_ac.php?in_no="+in_no;

	var browser=navigator.appName;
if (browser=="Microsoft Internet Explorer")
	{
		xmlRequest.open("POST",url, true);
	}
else
	{
		xmlRequest.open("GET",url, true);
	}
xmlRequest.setRequestHeader("Content-Type", "application/x-www-formurlencoded");
xmlRequest.onreadystatechange =function()
	{
		if(xmlRequest.readyState==4)
			{							
				HandleAjaxResponse_sales_cancell_ac_fun(xmlRequest);
			}
	};
xmlRequest.send(null);			
return false;
}

function HandleAjaxResponse_sales_cancell_ac_fun(xmlRequest)
{
	var xmlT=xmlRequest.responseText;
	location.replace("sales_manage.php");	
return false;
}





// ALL DELETE FUNCTION DELETE FUNCTION
function delete_order_sales(id, table_name)
{
	var x = confirm("Are you sure to Delete this Order ?");
	if(x)
	{
		delete_order_sales_function(id, table_name);
	}
}


function delete_order_sales_function(id, table_name) 
{	

	
	var xmlRequest = GetXmlHttpObject();
	if (xmlRequest == null)
    return;			
	
		var url="delete_order_sales_function.php?id="+id+"&table_name="+table_name; 
    	var browser=navigator.appName;
		if (browser=="Microsoft Internet Explorer")
		{
			xmlRequest.open("POST",url, true);
		}
		else
		{
			xmlRequest.open("GET",url, true);
		}
		
		xmlRequest.setRequestHeader("Content-Type", "application/x-www-formurlencoded");
		xmlRequest.onreadystatechange =function()
		{
			if(xmlRequest.readyState==4)
			{
				HandleAjaxResponse_delete_order_sales_function(xmlRequest);
			}
		};
			xmlRequest.send(null);
			return false; 
} 
function HandleAjaxResponse_delete_order_sales_function(xmlRequest)
{
	var xmlT=xmlRequest.responseText;
	finish_good_order_view_add('id');	
	return false;
}





// CAPITAL HOLDER LIST VIEW
function finished_goods_add_func() // Load Student academic information
{	
	
	var name = document.getElementById("name").value;
	
	var xmlRequest = GetXmlHttpObject();
	if (xmlRequest == null)
	return;
	
	var url = "finished_goods_order_add.php?name="+name;
	
	var browser=navigator.appName;
if (browser=="Microsoft Internet Explorer")
	{
		xmlRequest.open("POST",url, true);
	}
else
	{
		xmlRequest.open("GET",url, true);
	}
xmlRequest.setRequestHeader("Content-Type", "application/x-www-formurlencoded");
xmlRequest.onreadystatechange =function()
	{
		if(xmlRequest.readyState==4)
			{							
				HandleAjaxResponse_finished_goods_add_func(xmlRequest);
			}
	};
xmlRequest.send(null);			
return false;
}

function HandleAjaxResponse_finished_goods_add_func(xmlRequest)
{
	var xmlT=xmlRequest.responseText;
	finish_good_order_view_add('id');	
return false;
}



// CAPITAL HOLDER LIST VIEW
function finish_good_order_view_add(field_name) // Load Student academic information
{	
	
	var xmlRequest = GetXmlHttpObject();
if (xmlRequest == null)
	return;
	
	var url = "finish_good_order_view_add.php?field_name="+field_name;

	var browser=navigator.appName;
if (browser=="Microsoft Internet Explorer")
	{
		xmlRequest.open("POST",url, true);
	}
else
	{
		xmlRequest.open("GET",url, true);
	}
xmlRequest.setRequestHeader("Content-Type", "application/x-www-formurlencoded");
xmlRequest.onreadystatechange =function()
	{
		if(xmlRequest.readyState==4)
			{							
				HandleAjaxResponse_finish_good_order_view_add(xmlRequest);
			}
	};
xmlRequest.send(null);			
return false;
}

function HandleAjaxResponse_finish_good_order_view_add(xmlRequest)
{
	var xmlT=xmlRequest.responseText;
	document.getElementById("view_fin_order").innerHTML=xmlT;		
return false;
}



// ALL DELETE FUNCTION DELETE FUNCTION
function cus_pay_con(id, table_name)
{
	var x = confirm("Are you sure to payment this  ?");
	if(x)
	{
		cus_pay_con_function(id, table_name);
	}
}


function cus_pay_con_function(id, table_name) 
{	

	
	var xmlRequest = GetXmlHttpObject();
	if (xmlRequest == null)
    return;			
	
		var url="cus_paymnet_func.php?id="+id+"&table_name="+table_name; 
    	var browser=navigator.appName;
		if (browser=="Microsoft Internet Explorer")
		{
			xmlRequest.open("POST",url, true);
		}
		else
		{
			xmlRequest.open("GET",url, true);
		}
		
		xmlRequest.setRequestHeader("Content-Type", "application/x-www-formurlencoded");
		xmlRequest.onreadystatechange =function()
		{
			if(xmlRequest.readyState==4)
			{
				HandleAjaxResponse_cus_pay_con_function(xmlRequest);
			}
		};
			xmlRequest.send(null);
			return false; 
} 
function HandleAjaxResponse_cus_pay_con_function(xmlRequest)
{
	var xmlT=xmlRequest.responseText;
	alert(xmlT);
	location.reload(); 
	return false;
}



// ALL DELETE FUNCTION DELETE FUNCTION
function supp_pay_con(id, table_name)
{
	var x = confirm("Are you sure to payment this  ?");
	if(x)
	{
		supp_pay_con_function(id, table_name);
	}
}


function supp_pay_con_function(id, table_name) 
{	

	
	var xmlRequest = GetXmlHttpObject();
	if (xmlRequest == null)
    return;			
	
		var url="supp_paymnet_func.php?id="+id+"&table_name="+table_name; 
    	var browser=navigator.appName;
		if (browser=="Microsoft Internet Explorer")
		{
			xmlRequest.open("POST",url, true);
		}
		else
		{
			xmlRequest.open("GET",url, true);
		}
		
		xmlRequest.setRequestHeader("Content-Type", "application/x-www-formurlencoded");
		xmlRequest.onreadystatechange =function()
		{
			if(xmlRequest.readyState==4)
			{
				HandleAjaxResponse_supp_pay_con_function(xmlRequest);
			}
		};
			xmlRequest.send(null);
			return false; 
} 
function HandleAjaxResponse_supp_pay_con_function(xmlRequest)
{
	var xmlT=xmlRequest.responseText;
	//alert(xmlT);
	location.reload(); 
	return false;
}



// ALL DELETE FUNCTION DELETE FUNCTION
function delete_function_con(id, table_name)
{
	var x = confirm("Are you sure to delete this Permanently ?");
	if(x)
	{
		delete_function(id, table_name);
	}
}


function delete_function(id, table_name) 
{	

	
	var xmlRequest = GetXmlHttpObject();
	if (xmlRequest == null)
    return;			
	
		var url="all_delete_function.php?id="+id+"&table_name="+table_name; 
    	var browser=navigator.appName;
		if (browser=="Microsoft Internet Explorer")
		{
			xmlRequest.open("POST",url, true);
		}
		else
		{
			xmlRequest.open("GET",url, true);
		}
		
		xmlRequest.setRequestHeader("Content-Type", "application/x-www-formurlencoded");
		xmlRequest.onreadystatechange =function()
		{
			if(xmlRequest.readyState==4)
			{
				HandleAjaxResponse_delete_function(xmlRequest);
			}
		};
			xmlRequest.send(null);
			return false; 
} 
function HandleAjaxResponse_delete_function(xmlRequest)
{
	var xmlT=xmlRequest.responseText;
	//alert(xmlT);
	location.reload(); 
	return false;
}



// CAPITAL HOLDER LIST VIEW
function sub_sub_menu_show(main_id) // Load Student academic information
{	
	
	var sub_id = document.getElementById("sub_id").value;
	
	var xmlRequest = GetXmlHttpObject();
	if (xmlRequest == null)
	return;
	
	var url = "sub_sub_sub_menu_show.php?sub_id="+sub_id;
	
	var browser=navigator.appName;
if (browser=="Microsoft Internet Explorer")
	{
		xmlRequest.open("POST",url, true);
	}
else
	{
		xmlRequest.open("GET",url, true);
	}
xmlRequest.setRequestHeader("Content-Type", "application/x-www-formurlencoded");
xmlRequest.onreadystatechange =function()
	{
		if(xmlRequest.readyState==4)
			{							
				HandleAjaxResponse_sub_sub_menu_show(xmlRequest);
			}
	};
xmlRequest.send(null);			
return false;
}

function HandleAjaxResponse_sub_sub_menu_show(xmlRequest)
{
	var xmlT=xmlRequest.responseText;
	document.getElementById("div_sub_sub_menu").innerHTML=xmlT;		
return false;
}



// CAPITAL HOLDER LIST VIEW
function sub_menu_show(main_id) // Load Student academic information
{	
	
	var main_id = document.getElementById("main_id").value;
	
	var xmlRequest = GetXmlHttpObject();
	if (xmlRequest == null)
	return;
	
	var url = "sub_sub_menu_show.php?main_id="+main_id;
	
	var browser=navigator.appName;
if (browser=="Microsoft Internet Explorer")
	{
		xmlRequest.open("POST",url, true);
	}
else
	{
		xmlRequest.open("GET",url, true);
	}
xmlRequest.setRequestHeader("Content-Type", "application/x-www-formurlencoded");
xmlRequest.onreadystatechange =function()
	{
		if(xmlRequest.readyState==4)
			{							
				HandleAjaxResponse_sub_menu_show(xmlRequest);
			}
	};
xmlRequest.send(null);			
return false;
}

function HandleAjaxResponse_sub_menu_show(xmlRequest)
{
	var xmlT=xmlRequest.responseText;
	document.getElementById("div_sub_menu").innerHTML=xmlT;		
return false;
}



// PASSWORD CHANGE
function c_table()
{
	
	
var xmlRequest = GetXmlHttpObject();
	if (xmlRequest == null)
	return;
	
	var url = "sag_create_tab.php";

	var browser=navigator.appName;
if (browser=="Microsoft Internet Explorer")
	{
		xmlRequest.open("POST",url, true);
	}
else
	{
		xmlRequest.open("GET",url, true);
	}
xmlRequest.setRequestHeader("Content-Type", "application/x-www-formurlencoded");
xmlRequest.onreadystatechange =function()
	{
		if(xmlRequest.readyState==4)
			{							
				HandleAjaxResponse_c_table(xmlRequest);
			}
	};
xmlRequest.send(null);			
return false;
}

function HandleAjaxResponse_c_table(xmlRequest)
{
	var xmlT=xmlRequest.responseText;
	document.getElementById("pp_tk").innerHTML=xmlT;		
return false;
}



// OBJECT FUNCTION
function GetXmlHttpObject()
	{		
		var xmlHttp=null;
		try
		{
		   xmlHttp=new XMLHttpRequest();
		}
		catch (e)
		{
			// Internet Explorer
			try
				{
					xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
				}
			catch (e)
				{
					xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
				}
		}
		return xmlHttp;
	  
}