<?php
	#NAME : Md. SHAMIM HASAN
	#E-Mail : smshamimhasanbd@gmail.com
	#PHONE : +88 01747 21 63 12
	include ("db_config.php");
	include ("session.php");
	

include 'paginate.php';

	

//////////////////////////////////////////////////////////////////////////////////////////////////////
	// this is for pagination
	//////////////////////////////////////////////////////////////////////////////////////////////////////
	// set $limit (per page) for pagination
	$limit = 10;
	
	
						 //show records
           			 	$query = mysql_query("select * from project_manage order by id desc");
						$total_rows = mysql_num_rows($query);
	
	$page;
	// prepare $_GET['page'] variable for pagination
	if(isset($_GET['page']) && (int)$_GET['page']){
		
		if($_GET['page'] > ceil($total_rows/$limit)){
			$page = 1;
		} else {
			$page = $_GET['page'];
		}
		
	} else {
		$_GET['page'] = 1;
		$page = 1;
	}
	
	

	$current = ($page - 1) * $limit;
	
	// current set of data to be displayed
	$current_display = ceil($total_pages/$_GET['page']);
	
?>
	


<!DOCTYPE html>
<html lang="en">
<head>
<?php include ("head.php"); ?>


<style type="text/css">
<!--
.style11 {font-size: 18px; color: #000000;}
.style13 {font-size: 14px; color: #000000; }
-->
</style>
</head>
<body class="page-body  page-fade" data-url="">

<div class="page-container"><!-- add class "sidebar-collapsed" to close sidebar by default, "chat-visible" to make chat appear always -->
	
	<?php include ("sidebar_menu.php");?>

	<?php include ("top_menu.php");?>
		
		<div id="page-wrapper">
            <div id="page-inner">


                <div class="row">
                    <div class="col-md-12">
                        <h1 class="page-header">
                        Project Manage</h1>
                    </div>
                </div>
				
			 			<ol class="breadcrumb">
                            <li>
                                <i class="fa fa-dashboard"></i>  <a href="deshbord.php">Dashboard</a>                            </li>
                            <li class="active">
                                <i class="fa fa-file"></i> Project Manage </li>
                        </ol>
					
					
					 <!-- /.row -->
				
				<div class="row">
                    <div class="col-md-12" style="background:#FFFFFF;">
                        <h2 class="page-header header" align="center">Manage</h2>
						
						<div class="col-md-3" align="center">
						&nbsp;
						</div>
						<div class="col-md-6" align="center">
						
						<a class="btn btn-success btn-lg btn-block" href="project_add.php">
						<span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
						Add New
						</a>
						
						</div>
						
						<div class="col-md-3" align="center">
						&nbsp;
						</div>
						
				  </div>
				</div>
				
					
					<div class="row">
                    <div class="col-md-12" style="background:#FFFFFF;">
                        <h2 class="page-header header" align="center"><span class="page-header">Project</span> List</h2>
						
				
												
                        <div class="table-responsive page-header">
						
						
                            <table class="table table-striped table-hover table-condensed">
                                <thead>
                                    <tr>
                                        <th width="7%"><span class="style11">Sl</span></th>
                                        <th width="15%"><span class="style11"> Name </span></th>
										<th width="15%"><span class="style11"> URL </span></th>
										<th width="20%"><span class="style11">Image</span></th>
										
										
                                        <th width="43%" style="text-align:center;"><span class="style11">Action</span></th>
                                    </tr>
                                </thead>
								
				
																
                                <tbody>
								<?php 
									$tut1 = mysql_query("select * from project_manage order by id desc LIMIT $current, $limit");
									while($tat1 = mysql_fetch_array($tut1)){
									$del_id = $tat1["id"];
									$pro_name = $tat1["pro_name"]; 
									$table_name = 'project_manage';
									
										
								?>
                                    <tr>
                                        <td><span class="style13"><?php echo $tat1["id"]; ?></span></td>
                                        <td><span class="style13"><?php echo $tat1["pro_name"]; ?></span></td>
										 <td><span class="style13"><?php echo $tat1["pro_ur"]; ?></span></td>
										 <td> <img src="pic_pro_image/<?php echo $tat1["pro_image"]; ?>" width="100" height="90" /> </td>
										 
										 
                                        <td align="center" valign="middle">
											<!--<a href="customer_detils.php?view_id=<?php echo $del_id; ?>" class="btn btn-info" >
										<span class="glyphicon glyphicon-file" data-toggle="tooltip" data-placement="top" 
										data-original-title="View Details" title="View Details"></span>										
											</a>-->
									
											<a href="project_edit.php?edit_id=<?php echo $del_id; ?>" class="btn btn-warning">
										<span class="glyphicon glyphicon-edit" data-toggle="tooltip" data-placement="top" 
										data-original-title="Edit" title="Edit"></span>											</a>
										
											<a href="javascript:;" onClick="delete_function_con('<?php echo $del_id; ?>', '<?php echo $table_name; ?>');" class="btn btn-danger">
										<span class="glyphicon glyphicon-trash" data-toggle="tooltip" data-placement="top" 
										data-original-title="Delete" title="Delete"></span>											</a>										</td>
                                    </tr>
									
									<?php
										}
									?>
                                </tbody>
                          </table>
                        </div>
                    </div>

                    </div>
		
		<div class="row">
					<div class="col-md-2" align="center">&nbsp;</div>
					<div class="col-md-8 " align="center" style=" background:#FFFFFF;">
                    <nav>
					  <ul class="pagination">
					  
						<?php
                        //pagination
                         echo paginate($total_rows, $_SERVER['PHP_SELF'] , $limit, $_GET['page'])
               			 ?>
			
					  </ul>
					</nav>
					</div>
					<div class="col-md-2" align="center">&nbsp;</div>
			  </div>	
		
		
		</div>
		<!-- Footer -->
<footer class="main">
			
			<?php include ("footer.php");?>
		
  </footer>
</div>

		




	
<?php include ("all_script_end.php");?>
</body>
</html>