<?php
	#NAME : Md. SHAMIM HASAN
	#E-Mail : smshamimhasanbd@gmail.com
	#PHONE : +88 01747 21 63 12
	include ("db_config.php");
	include ("session.php");
	
?>
	


<!DOCTYPE html>
<html lang="en">
<head>
<?php include ("head.php"); ?>
</head>
<body class="page-body  page-fade" data-url="">

<div class="page-container"> 
	
	<?php include ("sidebar_menu.php");?>

	<?php include ("top_menu.php");?>
		
		 <div id="page-wrapper">
		 
            <div id="page-inner">


                <div class="row">
                    <div class="col-md-12">
                        <h1 class="page-header">
                            Project Add <small> New Project Add </small>
                        </h1>
                    </div>
                </div>
				
			 			<ol class="breadcrumb">
                            <li>
                                <i class="fa fa-dashboard"></i>  <a href="deshbord.php">Dashboard</a>                            </li>
							
							 <li>
                                <i class="fa fa-file"></i>  <a href="project_manage.php">Project Manage</a>                            </li>
							
                            <li class="active">project Add </li>
                        </ol>
		

                    <div class="col-md-12 col-sm-12 col-xs-12">

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Project Add
                            </div> 
                            <div class="panel-body">
                                <div class="table-responsive">
                                    
									
										
						
						<form class="form-horizontal" role="form" id="setting" name="post" method="post" action="project_add_ac.php" enctype="multipart/form-data">
						
						
							 
						
							 <div class="form-group">
								<label for="text" class="col-sm-2 control-label">Project Name   : </label>
								<div class="col-sm-10">
								  <input name="pro_name" type="text" class="form-control" id="text" placeholder="Enter Project Name  " >
								</div>
								
						  </div>
						
						
								  <div class="form-group">
								<label for="text" class="col-sm-2 control-label">Project URL  : </label>
								<div class="col-sm-10">
								  <input name="pro_ur" type="text" class="form-control" id="text" placeholder="Enter Project URL" >
								</div>
								
							  </div>
							  
							  <div class="form-group">
								<label for="text" class="col-sm-2 control-label">Project Image   : </label>
								<div class="col-sm-10">
								<div style="position:relative;">
									<a class='btn' href='javascript:;'>
										Picture Upload...
										<input name="pro_image" type="file" class="uploader" id="pro_image"  onchange='$("#upload-file-info").html($(this).val());' size="40">
									</a>
									&nbsp;
									<span class='label label-info' id="upload-file-info"></span>
								</div>
							  </div>
							  </div>
							  
					  
							 
						  <div class="form-group">
							<div class="col-sm-offset-2 col-sm-10">
							  <button type="submit" class="btn btn-success btn-lg">Save</button>
							</div>
						  </div>
												  
						  
						 
						</form>
						
									
									
                                </div>
                            </div>
                        </div>

                    </div>
                
				<!-- /. BODY CONTENT END  -->
				
				
<footer class="main">
			
			<?php include ("footer.php");?>
		
  </footer>
</div>

		




	
<?php include ("all_script_end.php");?>
</body>
</html>