<div class="sidebar-menu">

		<div class="sidebar-menu-inner">
			
			<header class="logo-env">

				<!-- logo -->
				<div class="logo">
					<a href="deshbord.php">
						<img src="logo/<?php echo $hasan1["s_logo"]; ?>" alt="Logo" width="120" class="img-responsive logo" />
					</a>
				</div>

				<!-- logo collapse icon -->
				<div class="sidebar-collapse">
					<a href="#" class="sidebar-collapse-icon"><!-- add class "with-animation" if you want sidebar to have animation during expanding/collapsing transition -->
						<i class="entypo-menu"></i>
					</a>
				</div>

								
				<!-- open/close menu icon (do not remove if you want to enable menu on mobile devices) -->
				<div class="sidebar-mobile-menu visible-xs">
					<a href="#" class="with-animation"><!-- add class "with-animation" to support animation -->
						<i class="entypo-menu"></i>
					</a>
				</div>

			</header>
			
									
			<ul id="main-menu" class="main-menu">
				<!-- add class "multiple-expanded" to allow multiple submenus to open -->
				<!-- class "auto-inherit-active-class" will automatically add "active" class for parent elements who are marked already with class "active" -->
				<li class="active opened active">
					<a href="deshbord.php">
						<i class="entypo-gauge"></i>
						<span class="title">Dashboard</span>
					</a>
					
				</li>
				
										
				<li>
						<a href="javascript:;" data-toggle="collapse" data-target="#Income">
							<i class="entypo-doc-text"></i> Menu Manage
						</a>
							<ul id="Menu" class="collapse">
							
								<li><a href="menu_manage.php">Menu Manage</a> </li>
								<li><a href="sub_menu_manage.php">Sub Menu Manage</a> </li>
							</ul>
				</li>
		
					<li>
					<a href="slider_manage.php">
						<i class="entypo-layout"></i>
						<span class="title">Slider Manage</span>
					</a>
					</li>	
					
					<li>
						<a href="javascript:;" data-toggle="collapse" data-target="#Income">
							<i class="fa fa-plus-square"></i> Advertisement Manage 
						</a>
								<ul id="advertisement" class="collapse">
							
<!--									<li><a href="top_ad_manage.php">Top Ad Manage</a> </li>-->
									<li><a href="banner_ad_manage.php">Banner Ad Manage</a> </li>
									<li><a href="left_out_side_manage.php">Left OutSide Ad</a> </li>
									<li><a href="right_side_ad_manage.php">Right Side Ad</a> </li>
									<li><a href="right_out_side_manage.php">Right OutSide Ad</a> </li>
									<li><a href="content_ad_1_manage.php">Content  Ad 1</a> </li>
									<li><a href="content_ad_2_manage.php">Content  Ad 2</a> </li>
<!--
									<li><a href="post_top_ad_manage.php">Post Top Ad</a> </li>
									<li><a href="post_bottom_ad_manage.php">Post Bottom Ad</a> </li>
									<li><a href="fotter_ad_manage.php">Footer Ad Manage</a> </li>
							
-->
							 
								</ul>
					</li>
					
					<li>		
					<a href="#clients_manage.php">
						<i class="glyphicon glyphicon-align-justify"></i>
						<span class="title">Banner Manage</span>
					</a>
				</li>
					
					<li>
					<a href="news_manage.php">
						<i class="entypo-newspaper"></i>
						<span class="title">News Manage</span>
					</a>
					</li>

					<li>
					<a href="servay.php">
						<i class="entypo-newspaper"></i>
						<span class="title">News Servay</span>
					</a>
					</li>
					
								
				<li>
					<a href="javascript:;" data-toggle="collapse" data-target="#Income">
						<i class="glyphicon glyphicon-film"></i> Gallery Manage
					</a>
						<ul id="Menu" class="collapse">
							
							<li><a href="pho_galry_manage.php">Photo List</a> </li>
							<li><a href="video_manage.php">Video List</a> </li>
						</ul>
				</li>
				
					
				
				<li>
					<a href="user_manage.php">
						<i class="entypo-user"></i>
						<span class="title">User Manage</span>
					</a>
				</li>	
					
			<!--		
					<li>
					<a href="contact_manage.php">
						<i class="entypo-mail"></i>
						<span class="title">Contact Manage</span>
					</a>
					</li>	
					
					
					<li>
					<a href="massage_manage.php">
						<i class="entypo-forward"></i>
						<span class="title">Online Massage</span>
					</a>
					</li>	
					
					
					<li>
					<a href="order_manage.php">
						<i class="entypo-basket"></i>
						<span class="title">Order</span>
					</a>
					</li>	
					
					<li>
					<a href="http://n-solution.net:2095/" target="_blank">
						<i class="entypo-reply"></i>
						<span class="title">Webmail</span>
					</a>
					</li>	
					
					
					
						<li>
					<a target="_blank" href="https://s2.mylivechat.com/webconsole/">
						
						<i class="entypo-chat"></i>
						<span class="title">Live Chat</span>
					</a>
					</li>	
				
				
				
			
				
					
				<li>
					<a href="#">
						<i class="entypo-layout"></i>
						<span class="title">Sub Menu Main</span>
					</a>
					<ul>
						
						<li>
							<a href="#">
								<span class="title">Sub Menu(Right Sidebar)</span>
							</a>
						</li>
						<li>
							<a href="#">
								<span class="title">Sub Sub Menu main</span>
							</a>
							<ul>
								
								<li>
									<a href="#l">
										<span class="title">Sub Sub Menu</span>
									</a>
								</li>
								<li>
									<a href="#">
										<span class="title">Sub Sub Menu</span>
									</a>
								</li>
							</ul>
						</li>
						<li>
							<a href="layout-boxed.html">
								<span class="title">Boxed Layout</span>
							</a>
						</li>
					</ul>
				</li>
				
				
			<li>
					<a href="404.php">
						<span class="title">404 Page</span>
					</a>
			</li>
						
						
						
			</ul>-->
			
		</div>

	</div>