<?php
	#NAME : Md. SHAMIM HASAN
	#E-Mail : smshamimhasanbd@gmail.com
	#PHONE : +88 01747 21 63 12
	include ("db_config.php");
	include ("session.php");
	
?>
	


<!DOCTYPE html>
<html lang="bn">
<head>
<?php include ("head.php"); ?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"></head>
<body class="page-body  page-fade" data-url="">

<div class="page-container"> 
	
	<?php include ("sidebar_menu.php");?>

	<?php include ("top_menu.php");?>
		
		 <div id="page-wrapper">
		 
            <div id="page-inner">


                <div class="row">
                    <div class="col-md-12">
                        <h1 class="page-header">
                            User Add <small> New User Add </small>                        </h1>
                  </div>
                </div>
				
			 			<ol class="breadcrumb">
                            <li>
                                <i class="fa fa-dashboard"></i>  <a href="deshbord.php">Dashboard</a>                            </li>
							
							 <li>
                                <i class="fa fa-file"></i>  <a href="user_manage.php">User Manage</a>                            </li>
							
                            <li class="active">User Add </li>
                        </ol>
		

                    <div class="col-md-12 col-sm-12 col-xs-12">

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                User Add
                            </div> 
                            <div class="panel-body">
                                <div class="table-responsive">
                                    
									
										
						
						<form class="form-horizontal" role="form" id="setting" name="post" method="post" action="user_add_ac.php" enctype="multipart/form-data">
						
							<div class="form-group">
								<label for="text" class="col-sm-2 control-label"> পুরো নাম  : </label>
								<div class="col-sm-10">
								  <input name="full_name" type="text" class="form-control" id="menu_name" placeholder="ইউজার পুরো নাম লিখুন " >
								</div>
								
						  </div>
						  
						    <div class="form-group">
								<label for="text" class="col-sm-2 control-label"> ইউজার নাম : </label>
								<div class="col-sm-10">
								  <input name="user_name" type="text" class="form-control" id="menu_name" placeholder="ইউজার নাম লিখুন " >
								</div>
								
						  </div>
						  
						  <div class="form-group">
								<label for="text" class="col-sm-2 control-label"> ইউজার পাসওয়ার্ড  : </label>
								<div class="col-sm-10">
								  <input name="user_pass" type="password" class="form-control" id="menu_name" placeholder="ইউজার পাসওয়ার্ড লিখুন " >
								</div>
								
						  </div>

						  
						  <div class="form-group">
								<label for="text" class="col-sm-2 control-label">ইউজার ইমেইল  : </label>
								<div class="col-sm-10">
								  <input name="user_email" type="text" class="form-control" id="menu_name" placeholder="ইউজার ইমেইল লিখুন " >
								</div>
								
						  </div>
						  
						<div class="form-group">
								<label for="text" class="col-sm-2 control-label"> ইউজার মোবাইল : </label>
								<div class="col-sm-10">
								  <input name="user_mobile" type="text" class="form-control" id="user_mobile" placeholder="ইউজার মোবাইল লিখুন " >
								</div>
								
						  </div>
						  
						  						  
						 <div class="form-group">
							<div class="col-sm-offset-2 col-sm-10">
							  <button type="submit" class="btn btn-success btn-lg">লিপিবদ্ধ করুন </button>
							</div>
						  </div>
						
						</form>
						
									
									
                                </div>
                            </div>
                        </div>

                    </div>
                
				<!-- /. BODY CONTENT END  -->
				
				
<footer class="main">
			
			<?php include ("footer.php");?>
		
  </footer>
</div>

		




	
<?php include ("all_script_end.php");?>
</body>
</html>