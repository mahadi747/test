<div class="row footer">
    
    <div class="col-md-12">
        <div class="row tiny-footer">
            <div class="col-md-4 col-lg-4 col-sm-4 weather">
                <!--<span id="weather" class="pull-left"></span>-->
                <span id="" class="weather-icon pull-right"><i class="fa fa-mixcloud" aria-hidden="true"></i></span>
            </div>
            <!-- wether -->
            <div class="col-md-4 col-lg-4 col-sm-4 app-link">
                <a href="javascript:" class="android"><i class="fa fa-android" aria-hidden="true"></i></a>
                <a href="javascript:" class="apple"><i class="fa fa-apple" aria-hidden="true"></i></a>
                <a href="javascript:" class="windows"><i class="fa fa-windows" aria-hidden="true"></i></a>
                <a href="javascript:" class="rss"><i class="fa fa-rss" aria-hidden="true"></i></a>
            </div>
            <!-- end app -->
            <div class="col-md-4 col-lg-4 col-sm-4 social-link">
                <a href="javascript:" class="facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                <a href="javascript:" class="twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                <a href="javascript:" class="youtube"><i class="fa fa-youtube-play" aria-hidden="true"></i></a>
                <a href="javascript:" class="linkedin"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
                <a href="javascript:" class="google-plus"><i class="fa fa-google-plus" aria-hidden="true"></i></a>
                <a href="javascript:" class="printarest"><i class="fa fa-pinterest-p" aria-hidden="true"></i></a>
            </div>
            <!-- social link -->
        </div>
    </div>
    <div class="col-md-12 f_second_part">
        <div class="row">
            <div class="col-md-4 col-sm-4 col-lg-4 footer-logo">
                <img src="css/img/Mohonasongbad.png" alt="" title=""/>
                <br>&copy; সর্বস্বত্ব স্বত্বাধিকার সংরক্ষিত মোহনা সংবাদ ২৪ ডট কম ২০১৪ - ২০১৭	
                  <br><p>প্রকাশক ও প্রধান সম্পাদক: তাওহীদুল হক ( লিটন )</p>
            </div>
            <div class="col-md-8 col-sm-8 col-lg-8 address">
                <p>বার্তা ও বাণিজ্যিক কার্যালয় : গুলিস্তান শপিং কমপ্লেক্স (৬ষ্ঠ তলা), রুম
                নং ০১, ঢাকা ১০০০।</p>
                <p>ওয়েবসাইটঃ www.mohonasongbad24.com</p>
                <p>ই-মেইল: news@mohonasongbad24.com</p>
                <p>মোবাইল : ০১৭১২-৭৪৪০৪৬</p>
                <p>বার্তা বিভাগ: ফোন: +৮৮ ০২-৯৫৬৮৭১৩,   ফ্যাক্স: +৮৮ ০২-৯৫৬৮৭১০  </p>
            </div>
        </div>
    </div>
    <!-- end extra ordinary-->

</div>

<!--footer-->

</div>
</div>
<script src="js/jquery-3.1.1.min.js" type="text/javascript"></script>
<script src="js/bootstrap.min.js" type="text/javascript"></script>
<script src="js/owl.carousel.min.js" type="text/javascript"></script>
<script src="js/jquery.simpleWeather.min.js" type="text/javascript"></script>
<script src="js/lightslider.js"></script>
<script src="js/lightbox.min.js"></script>
<script src="js/custom.js"></script>

</body>
</html>
