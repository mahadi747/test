<?php
	#NAME : Md. SHAMIM HASAN
	#E-Mail : smshamimhasanbd@gmail.com
	#PHONE : +88 01747 21 63 12
	include("../exsit-admin/db_config.php");
	include ("session.php");
	
	


?>

<!DOCTYPE html>
<html lang="en">
<head>
<?php include ("head.php"); ?>


</head>
<body class="page-body  page-fade" data-url="">

<div class="page-container"><!-- add class "sidebar-collapsed" to close sidebar by default, "chat-visible" to make chat appear always -->
	
	<?php include ("sidebar_menu.php");?>

	<?php include ("top_menu.php");?>
		
		<hr />
		
		<br />
		
				<div class="row">
					
					<ol class="breadcrumb">
							<li>
								<a href="#">
									<i class="entypo-folder"></i>
									Dashboard								</a>							</li>
							
							<li class="active">Edit Profile</li>
						</ol>
						
					<div class="col-md-12 col-sm-12 col-xs-12">

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Setting 
                            </div> 
                            <div class="panel-body">
                                <div class="table-responsive">
                                    
								
										
						<form class="form-horizontal" role="form" id="setting" name="post" method="post" action="setting_update_ac.php" enctype="multipart/form-data">
							  <div class="form-group">
							<label for="text" class="col-sm-2 control-label">Site Title: </label>
							<div class="col-sm-10">
							  <input name="s_title" type="text" class="form-control" id="text"  value="<?php echo $hasan1["s_title"]; ?>" >
							</div>
							
						  </div>
						  
						  <div class="form-group">
							<label for="text" class="col-sm-2 control-label">Site Name : </label>
							<div class="col-sm-10">
							  <input name="s_com_name" type="text" class="form-control" id="text"  value="<?php echo $hasan1["s_com_name"]; ?>">
							</div>
						  </div>
						  
						  <div class="form-group">
							<label for="text" class="col-sm-2 control-label">Address : </label>
							<div class="col-sm-10">
							  <input name="com_address" type="text" class="form-control" id="text"  value="<?php echo $hasan1["com_address"]; ?>">
							</div>
						  </div>
						  
						   <div class="form-group">
							<label for="text" class="col-sm-2 control-label">Site Logo : </label>
							<div class="col-sm-10">
							  	
								<div style="position:relative;">
								
									<a class='' href='javascript:;'>
										Logo Upload...
										<span class="btn btn-white btn-file">
					<input name="logo" type="file" class="uploader" id="logo"  onchange='$("#upload-file-info").html($(this).val());' size="40">
										<span class="fileinput-new">Select image</span>
									</a>
									&nbsp;
									
												
												
								</div>
								
							  <br />
							  <img src="logo/<?php echo $hasan1["s_logo"]; ?>" alt="Logo." class="img-thumbnail" style="background:#09192a;">
							</div>
						  </div>
						  
						  
						  
						  
						   <div class="form-group">
							<label for="text" class="col-sm-2 control-label">Meta Keywords : </label>
							<div class="col-sm-10">
							  <input name="m_key" type="text" class="form-control" id="text"  value="<?php echo $hasan1["m_key"]; ?>" >
							</div>
						  </div>
						  
						   <div class="form-group">
							<label for="text" class="col-sm-2 control-label">Meta Description : </label>
							<div class="col-sm-10">
							  <input name="m_desc" type="text" class="form-control" id="text"  value="<?php echo $hasan1["m_desc"]; ?>" >
							</div>
						  </div>
						  
						  <div class="form-group has-success">
							<label for="text" class="col-sm-2 control-label">Status : </label>
							<div class="col-sm-10">
							   <input type="text" class="form-control" id="inputSuccess1"  value="<?php echo $hasan1["status"]; ?>" readonly>
							</div>
						  </div>
						  
						  <div class="form-group">
							<div class="col-sm-offset-2 col-sm-10">
							  <button type="submit" class="btn btn-primary btn-lg">Update Setting</button>
							</div>
						  </div>
												  
						  
						 
						</form>
						
						
									
									
                                </div>
                            </div>
                        </div>

                    </div>
					
					
					
				</div>
		
		
		<br />
		<br />
		
		
		
		
		
		
		
		<!-- Footer -->
	<?php include ("footer.php");?>
	<!-- Footer -->

</div>

		




<?php include ("all_script_end.php");?>
</body>
</html>