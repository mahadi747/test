<?php session_start();?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<html lang="bn">
<head>

<?php include "exsit-admin/db_config.php"; ?>

    
    
    
    
    
    
    
    <?php 
    ////// view counting
    $news_title=$_REQUEST['news'];
    $sql=mysql_query("select * from news_manage where link_title='$news_title'");
    $data=mysql_fetch_assoc($sql);
    $main_menu_id=$data['menu_name'];
    $main_news_id=$data['id'];
    $user_ip=$_SERVER[''];

    $view=mysql_query("insert into view_count(news_id,viewer_ip,response) values('$news_id','$user_ip','1')");
    
    /// end view counting
    
    $selected_news=$_REQUEST['news'];
    $sql=mysql_query("select * from news_manage inner join menu_manage on news_manage.menu_name=menu_manage.id where news_manage.link_title='$selected_news'") or die(mysql_error());
    $data=mysql_fetch_assoc($sql);
    $menu_id=$data['menu_name'];
    $sub_menu_id=$data['sub_menu_name'];
    $news_id=$data['id'];
    $journalist_id=$data['reporter_id'];
    $journalist_sql=mysql_query("select * from journalist where id='$journalist_id'") or die(mysql_error());
    $journalist_data=mysql_fetch_assoc($journalist_sql);
    
    
    
?>
    
<?php include "head.php"; ?>
<meta name="keywords" content=""/>
<meta name="description" content="<?php echo $data['short_desc'];?>"/>
<meta name="title" content="<?php echo $data['title_news']; ?>"/>
<meta property="og:url"           content="<?php echo $base_url;?>news/<?php echo $news_title;?>" />
<meta property="og:title"         content="<?php echo $data['title_news']; ?>" />
<meta property="og:description"   content="<?php echo $data['short_desc'];?>" />
<meta property="og:image"         content="<?php echo $base_url;?>exsit-admin/news_img/<?php echo $data["news_img"]; ?>" />
<meta property="og:image:width" content="400" />
<meta property="og:image:height" content="300" />
</head>

<body>
<div class="container">
<div class="box-layout">
<!--header-->
<?php include "header.php"; ?>
<!--header--> 

<!--------------menu--------------->
<?php include "main_menu.php"; ?>
<!--------------menu--------------->

    <div class="row">
        <div class="content col-md-8">
            <div class="row visited-location">
                <div class="col-md-2 here-now"> Here Now </div>
                <div class="col-md-10 here-now-location">
                    <a href="index.php">হোম</a> <span><i class="fa fa-angle-double-right" aria-hidden="true"></i></span>
                    <a href="page/<?php echo $data['menu_name'] ?>"><?php echo $data['menu_name'] ?></a> <span><i class="fa fa-angle-double-right" aria-hidden="true"></i></span>
                    <a href="news/<?php echo $news_title;?>"><?php echo $news_title?></a> <span><i class="fa fa-angle-double-right" aria-hidden="true"></i></span>
                </div>
            </div>
            <!-- end visited location -->
            <div class="row news-details">
                <h1 class="news-title col-md-12"><?php echo $data['title_news']; ?></h1>
                <span class="news-label col-md-12"><?php echo $journalist_data['name'] ;?>, <?php echo $journalist_data['duty_place'] ;?> | 
                    আপডেট:
                    <?php ////date and time convert bangla
                        $date_time=$data['date'];
                        $symb_add=str_replace(" ","-",$date_time);
                        $symb_add=str_replace(":","-",$symb_add);
                        $make_array=explode("-",$symb_add);
                        $bangla_month=array('জানুয়ারী','ফেব্রুয়ারী','মার্চ','এপ্রিল','মে','জুন','জুলাই','আগস্ট','সেপ্টেম্বর','অক্টোবর','নভেম্বর','ডিসেম্বর');
                        $y=$make_array[0];
                        $m=$make_array[1]-1;
                        $d=$make_array[2];
                        $h=$make_array[3];
                        $i=$make_array[4];
                        $s=$make_array[5];
                        $english_number=array(0,1,2,3,4,5,6,7,8,9);
                        $bangla_number=array('০','১','২','৩','৪','৫','৬','৭','৮','৯');
                        $changable_date=$h.":".$i.", ".$bangla_month[$m]." ".$d.", ".$y;
                        echo $bangla_format_date_time=str_replace($english_number,$bangla_number,$changable_date);
                    ?>
                </span>
                <div class="col-md-12 like-button-area">
<!--                    <a href="#" class="pull-left">0 <span><i class="fa fa-thumbs-o-up" aria-hidden="true"></i></span></a>-->
                    <div class="pull-left">
                        <a href="https://www.facebook.com/sharer.php?u=<?php echo $base_url;?>news/<?php echo $news_title;?>" class="facebook-icon"><i class="fa fa-facebook" aria-hidden="true"></i>
                        </a>
                        <a href="https://twitter.com/intent/tweet?text=<?php echo $data['short_desc']; ?>&url=<?php echo $base_url;?>news/<?php echo $news_title;?>" class="twitter-icon"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                        <a href="https://plus.google.com/share?url=<?php echo $base_url;?>news/<?php echo $news_title;?>" class="google-plus-icon"><i class="fa fa-google-plus" aria-hidden="true"></i></a>
                        <a href="javascript:" onclick="print()" class="print-icon"><i class="fa fa-print" aria-hidden="true"></i></a>
                    </div>
                    <div class="pull-right">
                        
                        <a href="javascript:" class="zoom-out-icon"><i class="fa fa-search-minus" aria-hidden="true"></i></a>
                        <a href="javascript:" class="zoom-in"><i class="fa fa-search-plus" aria-hidden="true"></i></a>
                    </div>
                </div>
                <!-- end like button area with extra feature -->
                <div class="col-md-12 news-details">
                    <a href="exsit-admin/news_img/<?php echo $data["news_img"]; ?>" data-lightbox="<?php echo $data["news_img"]; ?>" data-title="<?php echo $data["title_news"]; ?>">
                    <img src="exsit-admin/news_img/<?php echo $data['news_img'];?>" alt="" title="" width="100%"/>
                    </a>
                    <?php echo $data['descriptions'];?>
                </div>
                <!-- end news details -->
                <div class="col-md-12 section-border">
                </div>
                <?php if($_SESSION['user_name']!=""):?>
                <div class="col-md-12">
                    <div class="detailBox">
                        <div class="titleBox">
                          <label>Comment Box</label>
                            <button type="button" class="close" aria-hidden="true"></button>
                        </div>
                        <div class="commentBox">

                            <p class="taskDescription">আমি <a href="rules.php">নীতিমালা</a> মেনে মন্তব্য করছি। </p>
                        </div>
                        <div class="actionBox">
                            <ul class="commentList">
                                <?php 
                                    $sql=mysql_query("select * from comments where news_id='$news_id'  order by id desc");
                                    while($data=mysql_fetch_assoc($sql)){
                                ?>
                                <?php 
                                    $user_id=$data['user_id'];
                                ?>
                                <li class="row">
                                    <div class="commenterImage col-md-2">
                                        <?php 
                                            $user_sql=mysql_query("select * from user_manage where id='$user_id'");
                                            $user_data=mysql_fetch_assoc($user_sql);
                                            $gender=$user_data['gender'];
                                            if($gender=="male"){
                                                $user_photo="img/boy-avatar.png";
                                            }
                                            elseif($gender=="female"){
                                                $user_photo="img/girl-avatar.png";
                                            }

                                        ?>
                                      <img src="<?php echo $user_photo?>" />
                                    </div>
                                    <div class="commentText col-md-10">
                                        <h5 class="commenter"><?php echo $user_data['full_name'] ?></h5>
                                        <p class=""><?php echo $data['comment'];?></p>
                                        <span class="date sub-text">on <?php echo $data['time'];?></span>
                                    </div>
                                </li>
                                <?php } ?>
                                
                            </ul>
                            <div class="form-inline">
                                <div class="form-group">
                                    <?php
                                        $user_name=$_SESSION['user_name'];
                                        $user_sql=mysql_query("select * from user_manage where user_name='$user_name'");
                                        $user_data=mysql_fetch_assoc($user_sql);
                                        $user_id=$user_data['id'];
                                        $ip=$_SERVER['REMOTE_ADDR'];
                                    ?>
                                    <input class="form-control" type="text" placeholder="Your comments" id='comment' required/>
                                    <input class="form-control" type="hidden"  id='user_id' value="<?php echo $user_id;?>"/>
                                    <input class="form-control" type="hidden"  id='ip' value="<?php echo $ip?>"/>
                                    <input class="form-control" type="hidden" id='news_id' value="<?php echo $news_id;?>"/>
                                </div>
                                <div class="form-group">
                                    <button  type="button" class="btn btn-default" id="comment_button">Comment</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- end comment -->
                <?php endif ?>
                <div class="col-md-12 col-sm-12 col-lg-12 rating-section">
                    <div class="pull-left">
                        <?php if($_SESSION['user_name']==""):?>
                            <input type="hidden" value="<?php echo $data['link_title'];?>" name="post_title" id="post_title">
                            মন্তব্য করতে <button type="button" class="btn btn-primary zoom-layer">লগইন</button> করুন অথবা <a href="signup.php">নিবন্ধন</a> করুন
                        <?php endif ?>
                    </div>
                </div>
                <!-- end subject -->
                <div class="col-md-12">
                    <div class="fb-comments" data-href="http://localhost/mohona/news/<?php echo $news_title;?>" data-numposts="5"></div>
                </div>
                
                <div class="clearfix"></div>
                <div class="panel panel-default related-division-news">
                    <div class="panel-heading">
                        এ বিভাগের অন্যান্য সংবাদ 
                    </div>
                    <div class="panel-body">
                        <ul class="related_news_thumb row">
                            <?php 
                                $sql=mysql_query("select * from news_manage where menu_name='$main_menu_id' and id not in('$main_news_id')  order by id desc limit 6 ");
                                while($data=mysql_fetch_assoc($sql))
                            {?>
                            <li class="col-md-4">
                                <a  href="news/<?php echo $data['link_title'];?>">
                                <img src='exsit-admin/news_img/<?php echo $data["news_img"]; ?>'>
                                <i class="fa fa-share" aria-hidden="true"></i><?php echo $data['title_news'];?> 
                                </a></li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- end news details -->
        </div>
        <!-- end content -->
        <div class="col-md-4" style="margin-top:10px;">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 style="margin-top:0px;margin-bottom:0px;">অ্যাড বিভাগ</h3>
                </div>
                <div class="panel-body">
                    <div class="right-add-vertise">
                    <?php 
    					$queryad = mysql_query("select * from right_side_ad_manage");
    							while($ad = mysql_fetch_array($queryad)){ ?>
    												
    					<div class="right_side_ad">
                                                        
                            <?php echo $ad['right_side_ad_link'] ?>
                                                    
                        </div>
    				<?php } ?>
                </div>
                <!-- end advertise -->
                </div>
            </div>
            
        </div>
        <! -- end right side bar -->
    </div>
    <section class="popup-layer">
        <div class="popup-content">
            <div class="popup-close-area">
                <div class="popup-close">
                    <i class="fa fa-window-close-o" aria-hidden="true"></i>
                </div>
            </div>
            <div class="popup-details">

            </div>
        </div>
    </section>
    <div id="fb-root"></div>
    <script>
        (function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.9&appId=294933750890457";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
    </script>
<?php include("footer.php");?>


